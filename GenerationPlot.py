#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import random as rand
import seaborn as sns
import numpy as np

sns.set(style="white", palette="bright", color_codes=True)


file_pathResult = 'plot/result/plot'
calculateReliability = False
numberofGenerations = 300

df = pd.read_csv('Result/agregatedTest2.csv', sep=';')

for metric in ['deadline', 'resource', 'LB']:
    if metric == 'deadline':
        myytitle = 'deadline'
        metrictitle = 'deadline violation'
    if metric == 'resource':
        myytitle = 'Resource'
        metrictitle = 'Resource consumption'
    if metric == 'LB':
        myytitle = 'loadBalancing'
        metrictitle = 'load balancing degree'

    figtitleStr = metrictitle
    myxtitle = 'Experiment sizes'

    dfMetricNSGA2 = df.loc[df['algo'] == 'NSGA2', ['generation', metric]]
    dfMetricNSGA2v1 = df.loc[df['algo'] == 'NSGA2V1', ['generation', metric]]
    dfMetricNSGA2v2 = df.loc[df['algo'] == 'NSGA2V2', ['generation', metric]]

    dfMetricRequestRate = df.loc[df['requestRate'] == 'requestRate', ['generation', metric]]

    print("---------------------------------------")

    dfMetricNSGA2 = dfMetricNSGA2.replace([np.inf, -np.inf], 0)  # if inf then 0
    dfMetricNSGA2v1 = dfMetricNSGA2v1.replace([np.inf, -np.inf], 0)  # if inf then 0
    dfMetricNSGA2v2 = dfMetricNSGA2v2.replace([np.inf, -np.inf], 0)  # if inf then 0

    dfMetricKUBDeadline = dfMetricKUBDeadline.replace([np.inf, -np.inf], 0)

    print(dfMetricKUBDeadline[metric].min())
    minValuey = min(dfMetricNSGA2[metric].min(), dfMetricNSGA2v1[metric].min(),dfMetricNSGA2v2[metric].min())
    maxValuey = max(dfMetricNSGA2[metric].max(),dfMetricNSGA2v1[metric].max(),dfMetricNSGA2v2[metric].max())
    minLimity = minValuey - 0.1 * (maxValuey - minValuey)
    maxLimity = maxValuey + 0.1 * (maxValuey - minValuey)

    minLimity = max(0.0, minLimity)
    # minLimity = 0.0

    fig = plt.figure()

    ax = fig.add_subplot(111)

    ax.set_xlabel('Generations', fontsize=18)
    ax.set_ylabel(myytitle, fontsize=18)
    plt.gcf().subplots_adjust(left=0.15)
    ax.plot(dfMetricNSGA2, label='NSGA', linewidth=2.0)
    ax.plot(dfMetricNSGA2v1, label='NSGA-v1', linewidth=2.0, linestyle="-.")

    plt.legend(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)

    plt.grid()

    fig.savefig(file_pathResult + '/TestPlot.pdf')
    plt.close(fig)










