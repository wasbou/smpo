
import numpy as np
import random as random
import sys
import POPULATION as pop
import matplotlib.pyplot as plt3d
import SystemConfiguration as systemmodel
import copy
from datetime import datetime
import GA as gacore
from numba import jit, cuda


class NSGA2:

    def __init__(self, system, conf_):

        self.corega = gacore.GA(system, conf_)
        self.corega.generatePopulation3(self.corega.populationPt)

        # self.coregas = gascore.GA(system, conf_)
        # self.coregas.generatePopulation(self.corega.populationPt)


    # ******************************************************************************************
    # ---------------------------------NSGA-II Algorithm----------------------------------------
    # ******************************************************************************************
    def crowdingDistancesAssigmentsN(self, popT, front):

        for i in front:
            popT.crowdingDistances[i] = float(0)

        frontFitness = [popT.fitnessNormalized[i] for i in front]
        #print(f" front fitness {frontFitness}")
        #print(popT.fitness)

        for key in popT.fitnessNormalized[0]:
            if key not in  ["index","total","reliabilityInstance"] :
                orderedList = sorted(frontFitness, key=lambda k: k[key])
                popT.crowdingDistances[orderedList[0]["index"]] = float('inf') # initialize
                minObj = orderedList[0][key]
                popT.crowdingDistances[orderedList[len(orderedList) - 1]["index"]] = float('inf')
                maxObj = orderedList[len(orderedList) - 1][key]
                # print(minObj,maxObj)
                normalizedDenominator = float(maxObj - minObj)
                # print(f"dominator {normalizedDenominator}")
                if normalizedDenominator == 0.0:
                    normalizedDenominator = float('inf')

                for i in range(1, len(orderedList) - 1):
                    # print("here",(orderedList[i + 1][key],orderedList[i - 1][key]))
                    popT.crowdingDistances[orderedList[i]["index"]] += (orderedList[i + 1][key] - orderedList[i - 1][key]) / normalizedDenominator

    def calculateCrowdingDistancesN(self, popT):
        i = 0
        while len(popT.fronts[i]) != 0:
            self.crowdingDistancesAssigmentsN(popT, popT.fronts[i])
            i += 1

    def crowdingDistancesAssigments(self, popT, front):

        for i in front:
            popT.crowdingDistances[i] = float(0)

        frontFitness = [popT.fitness[i] for i in front]
        #print(f" front fitness {frontFitness}")
        #print(popT.fitness)

        for key in popT.fitness[0]:
            if key not in  ["index","total","reliabilityInstance"] :
                # print(key)
                orderedList = sorted(frontFitness, key=lambda k: k[key])

                popT.crowdingDistances[orderedList[0]["index"]] = float('inf') # initialize
                minObj = orderedList[0][key]
                popT.crowdingDistances[orderedList[len(orderedList) - 1]["index"]] = float('inf')
                maxObj = orderedList[len(orderedList) - 1][key]
                normalizedDenominator = float(maxObj - minObj)
                # print(minObj,maxObj)
                # print(f"dominator {normalizedDenominator}")
                if normalizedDenominator == 0.0:
                    normalizedDenominator = float('inf')

                for i in range(1, len(orderedList) - 1):
                    popT.crowdingDistances[orderedList[i]["index"]] += (orderedList[i + 1][key] - orderedList[i - 1][key]) / normalizedDenominator

    def calculateCrowdingDistances(self, popT):
        i = 0
        while len(popT.fronts[i]) != 0:
            self.crowdingDistancesAssigments(popT, popT.fronts[i])
            i += 1

    def calculateDominants(self, popT):

        for i in range(len(popT.population)):
            popT.dominatedBy[i] = set()
            popT.dominatesTo[i] = set()
            popT.fronts[i] = set()

        for p in range(len(popT.population)):
            for q in range(p + 1, len(popT.population)):
                if self.corega.Impdominates(popT.fitness[p], popT.fitness[q]):
                    popT.dominatesTo[p].add(q)
                    popT.dominatedBy[q].add(p)
                if self.corega.Impdominates(popT.fitness[q], popT.fitness[p]):
                    popT.dominatedBy[p].add(q)
                    popT.dominatesTo[q].add(p)

    def calculateFronts(self, popT):

        addedToFronts = set()

        i = 0
        while len(addedToFronts) < len(popT.population):
            #print("dominatedBy",popT.dominatedBy)
            popT.fronts[i] = set([index for index, item in enumerate(popT.dominatedBy) if item == set()])
            #print("popT.fronts[i]",popT.fronts[i])
            addedToFronts = addedToFronts | popT.fronts[i] # set() or value "set contain the non dominated individu
            #print("addedToFronts",addedToFronts)

            for index, item in enumerate(popT.dominatedBy):
                if index in popT.fronts[i]:
                    popT.dominatedBy[index].add(-1) # front = -1
                else:
                    popT.dominatedBy[index] = popT.dominatedBy[index] - popT.fronts[i]
            i += 1

    def fastNonDominatedSort(self, popT):

        self.calculateDominants(popT)
        self.calculateFronts(popT)

    # ******************************************************************************************
    #   END NSGA-II Algorithm
    # ******************************************************************************************

    # ******************************************************************************************
    #   Evolution based on NSGA-II
    # ******************************************************************************************


    def evolveToOffspring(self):

        offspring = pop.POPULATION(self.corega.populationSize)
        offspring.population = []

        orderedFathers = self.crowdedComparisonOrder(self.corega.populationPt) # first fathers is equal to 0 the
        # print("orderedFathers")
        # print(orderedFathers)
        # offspring generation

        while len(offspring.population) < (self.corega.populationSize):
            father1 = self.corega.fatherSelection(orderedFathers)
            father2 = father1
            while father1 == father2:
                father2 = self.corega.fatherSelection(orderedFathers)
            # print("[Father selection]: Father1: %i **********************" % father1)
            # print("[Father selection]: Father1: %i **********************" % father2)

            self.corega.crossover(self.corega.populationPt.population[father1],self.corega.populationPt.population[father2], offspring.population)

        # offspring mutation

        for index, children in enumerate(offspring.population):
            if self.corega.rndEVOL.uniform(0, 1) < self.corega.mutationProbability:
                self.corega.mutate(children)


        return offspring

    def crowdedComparisonOrder(self, popT):
        valuesToOrder = []
        for i, v in enumerate(popT.crowdingDistances): # First croding disctance initialize
            citizen = {}
            citizen["index"] = i
            citizen["distance"] = v
            citizen["rank"] = 0
            valuesToOrder.append(citizen)

        f = 0
        #print("fronts")
        #print(popT.fronts)
        while len(popT.fronts[f]) != 0:
            for i, v in enumerate(popT.fronts[f]):
                valuesToOrder[v]["rank"] = f
            f += 1

        return sorted(valuesToOrder, key=lambda k: (k["rank"], -k["distance"]))

    def evolve(self):
        serviceDelay = 0
        deadlineViolation = 0
        totalRessource = 0
        reliabilityInstance = 0

        offspring = pop.POPULATION(self.corega.populationSize)
        offspring.population = []

        offspring = self.evolveToOffspring()

        self.corega.calculatePopulationFitnessObjectives(offspring)

        populationRt = offspring.populationUnion(self.corega.populationPt, offspring)
        # print(populationRt)
        # print("P")
        # print(self.corega.populationPt.population)
        # print("Q")
        # print(offspring.population)
        # print("RT")
        #print(populationRt.population)
        del self.corega.populationPt
        del offspring

        # print(f"Population  {populationRt.population}")
        # print(f"Fitness  {populationRt.fitness}")

        self.fastNonDominatedSort(populationRt)
        # print(f" front {populationRt.fronts}")
        self.calculateCrowdingDistances(populationRt)
        # print(f" crowding distance {populationRt.crowdingDistances}")

        orderedElements = self.crowdedComparisonOrder(populationRt) # give a rank to the front
        # print("orderedElements",orderedElements)

        finalPopulation = pop.POPULATION(self.corega.populationSize)


        for i in range(self.corega.populationSize): # get all individual ordred from the front
            finalPopulation.population[i] = copy.deepcopy(populationRt.population[orderedElements[i]["index"]])
            finalPopulation.fitness[i] = copy.deepcopy(populationRt.fitness[orderedElements[i]["index"]])


        del populationRt

        # minV = float('inf')
        # maxV = 0

        # print(f"Population final {finalPopulation.population}")
        # print(f"Fitness final {finalPopulation.fitness}")


        for i, v in enumerate(finalPopulation.fitness): # reorganize the index for the next population and normalized the objectives f
            finalPopulation.fitness[i]["index"] = i
            serviceDelay = finalPopulation.fitness[i]["serviceDelay"]
            totalResource = finalPopulation.fitness[i]["totalResource"]
            reliabilityInstance = finalPopulation.fitness[i]["reliabilityInstance"]

            if (finalPopulation.minDelay >= serviceDelay ) :
                finalPopulation.minDelay = serviceDelay

            if (finalPopulation.maxDelay <= serviceDelay and serviceDelay != float('inf')):
                finalPopulation.maxDelay = serviceDelay

            if (finalPopulation.minRessource >= totalResource):
                finalPopulation.minRessource = totalResource
            if (finalPopulation.maxRessource <= totalResource and totalResource != float('inf')):
                finalPopulation.maxRessource = totalResource

            if (finalPopulation.minAvailability >= reliabilityInstance):
                finalPopulation.minAvailability = reliabilityInstance
            if (finalPopulation.maxAvailability <= reliabilityInstance and reliabilityInstance != float('inf')):
                finalPopulation.maxAvailability = reliabilityInstance


        # print(finalPopulation)

        self.corega.populationPt = finalPopulation

        self.fastNonDominatedSort(self.corega.populationPt)
        self.calculateCrowdingDistances(self.corega.populationPt)

    def evolve2(self):

        deadlineViolation = 0
        totalRessource = 0
        reliabilityInstance = 0

        offspring = pop.POPULATION(self.corega.populationSize)
        offspring.population = []

        offspring = self.evolveToOffspring()

        self.corega.calculatePopulationFitnessObjectives(offspring)


        populationRt = offspring.populationUnion(self.corega.populationPt, offspring)
        # print("P")
        # print(self.corega.populationPt.fitness)
        # print("Q")
        # print(offspring.fitness)
        # print("RT")
        # print(populationRt.fitness)

        # del self.corega.populationPt
        # del offspring

        # print(f"Population  {populationRt.population}")
        #print(f"Fitness  {populationRt.fitness}")



        populationRt.NormalizedFunction()
        #print("wassim",populationRt.fitnessNormalized)

        self.fastNonDominatedSort(populationRt)
        # print(f" front {populationRt.fronts}")
        self.calculateCrowdingDistances(populationRt)
        # print(f" crowding distance {populationRt.crowdingDistances}")

        orderedElements = self.crowdedComparisonOrder(populationRt) # give a rank to the front
        # print("orderedElements",orderedElements)

        finalPopulation = pop.POPULATION(self.corega.populationSize)


        for i in range(self.corega.populationSize): # get all individual ordred from the front
            finalPopulation.population[i] = copy.deepcopy(populationRt.population[orderedElements[i]["index"]])
            finalPopulation.fitness[i] = copy.deepcopy(populationRt.fitness[orderedElements[i]["index"]])


        del populationRt

        # minV = float('inf')
        # maxV = 0

        # print(f"Population final {finalPopulation.population}")
        # print(f"Fitness final {finalPopulation.fitness}")

        finalPopulation.initNormalizedFunction()

        for i, v in enumerate(finalPopulation.fitness): # reorganize the index for the next population and normalized the objectives f
            finalPopulation.fitness[i]["index"] = i
            deadlineViolation = finalPopulation.fitness[i]["deadlineViolation"]
            totalResource = finalPopulation.fitness[i]["totalResource"]
            # reliabilityInstance = finalPopulation.fitness[i]["reliabilityInstance"]
            loadBalancing = finalPopulation.fitness[i]["loadbalancing"]

            if (finalPopulation.minloadBalancing >= loadBalancing ) :
                finalPopulation.minloadBalancing = loadBalancing

            if (finalPopulation.maxloadBalancing <= loadBalancing and loadBalancing != float('inf')):
                finalPopulation.maxloadBalancing = loadBalancing

            if (finalPopulation.minDeadlineViolation >= deadlineViolation ) :
                finalPopulation.minDeadlineViolation = deadlineViolation

            if (finalPopulation.maxDeadlineViolation<= deadlineViolation and deadlineViolation != float('inf')):
                finalPopulation.maxDeadlineViolation = deadlineViolation

            if (finalPopulation.minRessource >= totalResource):
                finalPopulation.minRessource = totalResource
            if (finalPopulation.maxRessource <= totalResource and totalResource != float('inf')):
                finalPopulation.maxRessource = totalResource

            if (finalPopulation.minAvailability >= reliabilityInstance):
                finalPopulation.minAvailability = reliabilityInstance
            if (finalPopulation.maxAvailability <= reliabilityInstance and reliabilityInstance != float('inf')):
                finalPopulation.maxAvailability = reliabilityInstance


        # print(finalPopulation)

        self.corega.populationPt = finalPopulation

        self.fastNonDominatedSort(self.corega.populationPt)
        self.calculateCrowdingDistances(self.corega.populationPt)

    def evolve3(self):

        deadlineViolation = 0
        totalRessource = 0
        reliabilityInstance = 0

        offspring = pop.POPULATION(self.corega.populationSize)
        offspring.population = []

        offspring = self.evolveToOffspring()

        self.corega.calculatePopulationFitnessObjectives(offspring)

        populationRt = offspring.populationUnion(self.corega.populationPt, offspring)
        # print("P")
        # print(self.corega.populationPt.fitness)
        # print("Q")
        # print(offspring.fitness)
        # print("RT")
        # print(populationRt.fitness)

        del self.corega.populationPt
        del offspring

        # print(f"Population  {populationRt.population}")
        # print(f"Fitness  {populationRt.fitness}")

        populationRt.NormalizedFunction()
        print("wassim",populationRt.fitnessNormalized)

        self.fastNonDominatedSort(populationRt)
        # print(f" front {populationRt.fronts}")
        self.calculateCrowdingDistancesN(populationRt)
        # print(f" crowding distance {populationRt.crowdingDistances}")

        orderedElements = self.crowdedComparisonOrder(populationRt)  # give a rank to the front
        # print("orderedElements",orderedElements)

        finalPopulation = pop.POPULATION(self.corega.populationSize)

        for i in range(self.corega.populationSize):  # get all individual ordred from the front
            finalPopulation.population[i] = copy.deepcopy(populationRt.population[orderedElements[i]["index"]])
            finalPopulation.fitness[i] = copy.deepcopy(populationRt.fitness[orderedElements[i]["index"]])

        del populationRt

        # minV = float('inf')
        # maxV = 0

        # print(f"Population final {finalPopulation.population}")
        # print(f"Fitness final {finalPopulation.fitness}")

        finalPopulation.initNormalizedFunction()

        for i, v in enumerate(
                finalPopulation.fitness):  # reorganize the index for the next population and normalized the objectives f
            finalPopulation.fitness[i]["index"] = i
            deadlineViolation = finalPopulation.fitness[i]["deadlineViolation"]
            totalResource = finalPopulation.fitness[i]["totalResource"]
            # reliabilityInstance = finalPopulation.fitness[i]["reliabilityInstance"]
            loadBalancing = finalPopulation.fitness[i]["loadbalancing"]

            if (finalPopulation.minloadBalancing >= loadBalancing):
                finalPopulation.minloadBalancing = loadBalancing

            if (finalPopulation.maxloadBalancing <= loadBalancing and loadBalancing != float('inf')):
                finalPopulation.maxloadBalancing = loadBalancing

            if (finalPopulation.minDeadlineViolation >= deadlineViolation):
                finalPopulation.minDeadlineViolation = deadlineViolation

            if (finalPopulation.maxDeadlineViolation <= deadlineViolation and deadlineViolation != float('inf')):
                finalPopulation.maxDeadlineViolation = deadlineViolation

            if (finalPopulation.minRessource >= totalResource):
                finalPopulation.minRessource = totalResource
            if (finalPopulation.maxRessource <= totalResource and totalResource != float('inf')):
                finalPopulation.maxRessource = totalResource

            if (finalPopulation.minAvailability >= reliabilityInstance):
                finalPopulation.minAvailability = reliabilityInstance
            if (finalPopulation.maxAvailability <= reliabilityInstance and reliabilityInstance != float('inf')):
                finalPopulation.maxAvailability = reliabilityInstance

        # print(finalPopulation)

        self.corega.populationPt = finalPopulation

        self.fastNonDominatedSort(self.corega.populationPt)
        self.calculateCrowdingDistancesN(self.corega.populationPt)


# ******************************************************************************************
#  END Evolution based on NSGA-II
# ******************************************************************************************



