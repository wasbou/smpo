import numpy as np
import random as random
import sys
import POPULATION as pop
import matplotlib.pyplot as plt3d
import copy
import math as math


class GAS:
    """
    Genetic Algorithm class
    Args:
        SystemConfiguration (SYSTEMMODEL instance): The system model information.
    """

    def __init__(self, system, conf):

        self.system = system
        self.cnf = conf
        self.populationSize = self.cnf.populationSize

        self.populationPt = pop.POPULATION(self.populationSize)
        self.mutationProbability = self.cnf.mutationProbability

        self.rndPOP = random.Random(self.cnf.populationSeed)
        self.rndEVOL = random.Random(self.cnf.evolutionSeed)



    # ------------------------- CROSSOVER -------------------------

    def crossoverMIO2(self, f1, f2, offs):

        c1 = list()
        c2 = list()
        cuttingPoint = self.rndEVOL.randint(1, self.system.fogNumber - 1)
        for i in range(0, self.system.serviceNumber):
            c1.append(f1[i][:cuttingPoint] + f2[i][cuttingPoint:])
            c2.append(f2[i][:cuttingPoint] + f1[i][cuttingPoint:])


        offs.append(c1)
        offs.append(c2)

        return c1, c2

    def crossover(self, f1, f2, offs=list()):

        return self.crossoverMIO2(f1, f2, offs)

    # ------------------------- END CROSSOVER -------------------------


    # ------------------------- MUTATIONS -------------------------

    def replicaIncreseValue(self, child, myvalue, numChanges):

        for i in iter(child):
            if self.rndEVOL.random() > 0.5:
                for j in range(numChanges):
                    indx = self.rndEVOL.randint(0, self.system.fogNumber - 1)
                    i[indx] = myvalue

    def replicaGrowth(self, child):
        self.replicaIncreseValue(child, 1, 1)


    def replicaShrink(self, child):
        self.replicaIncreseValue(child, 0, 1)


    def serviceShuffle(self, child):
        self.rndEVOL.shuffle(child)

    def serviceReplace(self, child):

        firstPoint = self.rndEVOL.randint(0, self.serviceInstanceChromosome - 1)
        secondPoint = self.rndEVOL.randint(0, self.serviceInstanceChromosome - 1)
        child[firstPoint], child[secondPoint] = child[secondPoint], child[firstPoint]

    def mutate(self, child):

        mutationOperators = []
        modifyNumberRnd = self.rndEVOL.random()
        # if (modifyNumberRnd < 1. / 3.):
        #     self.replicaGrowth(child)
        # if (modifyNumberRnd > 1. / 3.):
        #     self.replicaShrink(child)
        mutationOperators.append(self.replicaGrowth)
        mutationOperators.append(self.replicaShrink)
        mutationOperators.append(self.serviceShuffle)

        mutationOperators[self.rndEVOL.randint(0, len(mutationOperators) - 1)](child)

    # ------------------------- END MUTATIONS -------------------------


    # ------------------------- Dominate Solution -------------------------

    def dominates(self, a, b):
        # checks if solution a dominates solution b
        Adominates = True

        for key in a:
            ObjectivesKeys = ["deadlineViolation", "totalResource","loadbalancing"]
            if (key in ObjectivesKeys):
                if b[key] <= a[key]:
                    Adominates = False
                    break
            # elif(key == "reliabilityInstance"):
            #     if b[key] >= a[key]:
            #         Adominates = False
            #         break

        return Adominates

    # ------------------------- END Dominate -------------------------



    # ******************************************************************************************
    # Objective Function
    # ******************************************************************************************

    # -------------------------  Deadlien violation -------------------------
    def calculateDeadlineViolation(self, chromosome):

        TotalServiceDelay = 0
        UserServiceDelay = 0
        list_allocated_fog = []
        self.TotalServiceInstanceNumber = 0
        self.TotalAvailableInstancedict = {}
        self.deadlineUserViolation = 0
        self.TotalNeededInstances = 0
        self.UserViolation = 0

        for UserMs in self.system.UsersJ:  # for each user(service) in the system
            # print(UserMs)
            ServiceId = UserMs["device"]["serviceId"]  # get the serviceId requested by the user
            DataIn = UserMs["device"]["Data_in"]
            serviceRate = self.system.AppConfig[ServiceId]["resource"]["ServiceRate"]
            deadline = self.system.AppConfig[ServiceId]["resource"]["Deadline"]
            self.TotalAvailableInstance = []

            for indice, instance in enumerate(chromosome[ServiceId]):
                if (instance == 1):
                    list_allocated_fog.append(indice)

            allocated_fog_number = len(list_allocated_fog)  # length of allocated fog

            if (allocated_fog_number > 0):
                # ------------ Total service Instance number objective ----------------------
                self.TotalServiceInstanceNumber = self.TotalServiceInstanceNumber + sum(list_allocated_fog)

                # ---------------------------------------------------------------------------
                for user in range(len(UserMs["device"]["gatewayId"])):  # users requesting the serviceId
                    userGateway = UserMs["device"]["gatewayId"][user]
                    TotalAvailableInstanceUser = 0
                    AvailableInstance = 0
                    UserServiceDelay = 0
                    UserNeededInstance = UserMs["device"]["NeededInstance"][user]  # user needed Instance
                    deadlinecheck = 0
                    self.TotalNeededInstances = self.TotalNeededInstances + UserNeededInstance

                    UserAccessDelay = UserMs["device"]["AccessDelay"][user]  # Access delay
                    UserPocessingDelay = 1 / (serviceRate - (UserMs["device"]["Requestrate"][user] / UserNeededInstance))  # processing delay

                    # shortest path first for user
                    fog_service_dict = {k: v for k, v in sorted(self.system.allShortestPathLength[userGateway].items(), key=lambda x: x[1])}

                    fog_shortest_path_list = list(
                        fog_service_dict.keys())  # fog node index : list ordered keys based on shortest path
                    index = 0  # index begin with the shortest path first
                    UserShortestPath = self.system.allShortestPath[userGateway]  # the path [f1,f2..]
                    UserNetworkDelay = 0
                    allocated_fog_numberUser = allocated_fog_number
                    while allocated_fog_numberUser > 0:
                        # print("allocated_fog_number",allocated_fog_number)
                        fog_node = fog_shortest_path_list[index]
                        numberOf_fog = list_allocated_fog.count(fog_node)
                        if (numberOf_fog > 0):
                            n_node = len(UserShortestPath[fog_node])-1
                            UserNetworkDelay = fog_service_dict[fog_node] * DataIn + 2*n_node
                            #print("userGateway",userGateway,UserShortestPath[fog_node],UserNetworkDelay)
                            UserServiceDelay = UserPocessingDelay + UserNetworkDelay + UserAccessDelay

                            if UserServiceDelay <= deadline:  # calculate the total and available instances
                                # ------------ The availability of instance for each user ----------------------
                                TotalAvailableInstanceUser = TotalAvailableInstanceUser + numberOf_fog
                                # ---------------------------------------------------------------------------
                                for j in range(numberOf_fog):
                                    AvailableInstance = AvailableInstance + 1
                                    if (AvailableInstance <= UserNeededInstance):
                                        TotalServiceDelay = TotalServiceDelay + UserServiceDelay
                                        deadlinecheck = deadlinecheck + 1

                                    else:
                                        break
                            elif(deadlinecheck < UserNeededInstance and UserServiceDelay > deadline):
                                self.deadlineUserViolation = self.deadlineUserViolation + (UserServiceDelay-deadline)
                                self.UserViolation = self.UserViolation + 1
                                deadlinecheck = deadlinecheck + numberOf_fog

                            # print("user", userGateway,"first",fog_node, fog_service_dict, ",totalDelay:", TotalServiceDelay,"userdelay",UserServiceDelay, ",AccessDelay:",
                            #           UserAccessDelay, ",PocessingDelay:", UserPocessingDelay, ",NetworkDelay:",
                            #           UserNetworkDelay, ",AvailableInstance:", TotalAvailableInstanceUser)

                        allocated_fog_numberUser = allocated_fog_numberUser - numberOf_fog
                        index = index + 1


                    # print("AvailableInstance",AvailableInstance,"UserNeededInstance",UserNeededInstance)
                    if (allocated_fog_number < UserNeededInstance): # check if the needed instance is respected the the deadline is respected
                        return False
                    self.TotalAvailableInstance.append(TotalAvailableInstanceUser)
                    #print("ServiceId",ServiceId,"user",user,"available",TotalAvailableInstanceUser)

                list_allocated_fog = []
                self.TotalAvailableInstancedict[ServiceId] = self.TotalAvailableInstance
                # print(self.TotalAvailableInstancedict)
            else:
                self.TotalAvailableInstancedict[ServiceId] = [0 for i in range(len(UserMs["device"]["gatewayId"]))]
                return False
        # print("deadlineUserViolation",self.deadlineUserViolation)
        # print("TotalNeededInstances",self.TotalNeededInstances)
        return np.round(self.deadlineUserViolation,2)

    # ------------------------- Service Delay -------------------------
    def calculateServiceDelay(self, chromosome):

        TotalServiceDelay = 0
        UserServiceDelay = 0
        list_allocated_fog = []
        self.TotalServiceInstanceNumber = 0
        self.TotalAvailableInstancedict = {}
        for UserMs in self.system.UsersJ: # for each user(service) in the system
            #print(UserMs)
            ServiceId = UserMs["device"]["serviceId"] # get the serviceId requested by the user
            DataIn = UserMs["device"]["Data_in"]
            serviceRate= self.system.AppConfig[ServiceId]["resource"]["ServiceRate"]
            deadline = self.system.AppConfig[ServiceId]["resource"]["Deadline"]
            self.TotalAvailableInstance = []

            for k in range(self.system.arrayMaxNeededInstancesPositions[ServiceId],
                           self.system.arrayMaxNeededInstancesPositions[ServiceId + 1]):
                for indice, instance in enumerate(chromosome[k]):
                    if (instance == 1):
                        list_allocated_fog.append(indice)
            allocated_fog_number = len(list_allocated_fog) #length of allocated fog

            if ( allocated_fog_number > 0):
                #------------ Total service Instance number objective ----------------------
                self.TotalServiceInstanceNumber = self.TotalServiceInstanceNumber + sum(list_allocated_fog)

                #---------------------------------------------------------------------------
                for user in range(len(UserMs["device"]["gatewayId"])): # users requesting the serviceId
                    userGateway = UserMs["device"]["gatewayId"][user]
                    TotalAvailableInstanceUser = 0
                    AvailableInstance = 0
                    UserServiceDelay = 0
                    UserNeededInstance = UserMs["device"]["NeededInstance"][user]  # user needed Instance

                    UserAccessDelay = UserMs["device"]["AccessDelay"][user] # Access delay
                    UserPocessingDelay = 1/(serviceRate-(UserMs["device"]["Requestrate"][user]/UserNeededInstance)) # processing delay

                    # shortest path first for user
                    fog_service_dict = {k: v for k, v in sorted(self.system.allShortestPathLength[userGateway].items(), key=lambda x: x[1])}

                    fog_shortest_path_list = list(fog_service_dict.keys()) # fog node index : list ordered keys based on shortest path
                    index = 0 # index begin with the shortest path first

                    UserShortestPath = self.system.allShortestPath[userGateway] # the path [f1,f2..]

                    UserNetworkDelay = 0
                    allocated_fog_numberUser = allocated_fog_number
                    while allocated_fog_numberUser > 0:
                        #print("allocated_fog_number",allocated_fog_number)

                        fog_node = fog_shortest_path_list[index]
                        numberOf_fog = list_allocated_fog.count(fog_node)
                        if (numberOf_fog > 0) :
                            #n_node = UserShortestPath[fog_node]
                            n_node = len(UserShortestPath[fog_node]) - 1
                            UserNetworkDelay = fog_service_dict[fog_node] * DataIn + 2 * n_node

                            UserServiceDelay = UserPocessingDelay + UserNetworkDelay + UserAccessDelay

                            if UserServiceDelay <= deadline: # calculate the total and available instances
                                # ------------ The availability of instance for each user ----------------------
                                TotalAvailableInstanceUser = TotalAvailableInstanceUser + numberOf_fog
                                # ---------------------------------------------------------------------------
                                for j in range(numberOf_fog):
                                    AvailableInstance = AvailableInstance + 1
                                    if (AvailableInstance <= UserNeededInstance) :
                                        TotalServiceDelay = TotalServiceDelay + UserServiceDelay

                                    else:
                                        break

                            # print("user", userGateway,"first",fog_node, fog_service_dict, ",totalDelay:", TotalServiceDelay,"userdelay",UserServiceDelay, ",AccessDelay:",
                            #           UserAccessDelay, ",PocessingDelay:", UserPocessingDelay, ",NetworkDelay:",
                            #           UserNetworkDelay, ",AvailableInstance:", TotalAvailableInstanceUser)

                        allocated_fog_numberUser = allocated_fog_numberUser - numberOf_fog
                        index = index + 1

                    """
                    Constraint : Check if the needed instance is respected
                    """
                    #print("AvailableInstance",AvailableInstance,"UserNeededInstance",UserNeededInstance)
                    if (AvailableInstance < UserNeededInstance): # check if the needed instance is respected the the deadline is respected
                        return False
                    self.TotalAvailableInstance.append(TotalAvailableInstanceUser)
                    #print("ServiceId",ServiceId,"user",user,"available",TotalAvailableInstanceUser)


                list_allocated_fog = []
                self.TotalAvailableInstancedict[ServiceId]=self.TotalAvailableInstance
                #print(self.TotalAvailableInstancedict)
            else:
                return False
        return TotalServiceDelay
    # ------------------------- END Service Delay -------------------------


    # ------------------------- Resources Usage -------------------------


    def calculateTotalResources(self, chromosome):
        totalResources = 0
        numReplicas = 0
        for ServiceId in range(self.system.serviceNumber):
            for indice, instance in enumerate(chromosome[ServiceId]):
                numReplicas = numReplicas + np.sum(instance)

            totalResources = totalResources + (numReplicas * self.system.serviceResourcesCPU[ServiceId])
            numReplicas = 0
        return totalResources

    def calculateTotalResourcesN(self,chromosome):
        UsedFogNumber = []
        FogsCPUUtilisation = {}
        FogsRAMUtilisation = {}
        TotalResource = 0
        UsedFog = False
        fogCPUMean = 0
        fogRAMMean = 0
        fogCPUTotal = 0
        fogRAMTotal = 0
        for fog in range(self.system.fogNumber):
            fogCPU = 0
            fogRAM = 0
            for ServiceId in range(self.system.serviceNumber):
                if chromosome[ServiceId][fog] == 1:
                    UsedFog = True
                    fogCPU = fogCPU + self.system.serviceResourcesCPU[ServiceId]
                    fogRAM = fogRAM + self.system.serviceResourcesRAM[ServiceId]

            if(UsedFog):
                UsedFogNumber.append(fog)
                FogsCPUUtilisation[fog] = fogCPU / self.system.nodeCPU[fog]
                FogsRAMUtilisation[fog] = fogRAM / self.system.nodeRAM[fog]
            UsedFog = False
        UsedFogNumberLength = len(UsedFogNumber)

        #print("UsedFogNumber",UsedFogNumber,"totalServiceRate",totalServiceRate,"FogsServiceRate",FogsServiceRate)
        for fogId in UsedFogNumber:
            TotalResource = TotalResource + 0.5*FogsCPUUtilisation[fogId] + 0.5*FogsRAMUtilisation[fogId]
        return np.round(TotalResource / UsedFogNumberLength,2)

    # ------------------------- END Resources Usage -------------------------

    # ------------------------- Availability Cost -------------------------

    def calculateAvailability(self):
        availableCost = 0

        for UserMs in self.system.UsersJ: # for each user in the system
            ServiceId = UserMs["device"]["serviceId"] # get the serviceId requested by the user
            for user in range(len(UserMs["device"]["gatewayId"])): # users requesting the serviceId
                UserNeededInstance = UserMs["device"]["NeededInstance"][user]
                # print("user",user,"UserNeededInstance",UserNeededInstance)
                # print("user",user,"service",ServiceId,self.TotalAvailableInstancedict[ServiceId][user])
                if (self.TotalAvailableInstancedict[ServiceId][user] != 0 ):
                    availableCost = availableCost + (UserNeededInstance/self.TotalAvailableInstancedict[ServiceId][user])
                else:
                    availableCost = availableCost + UserNeededInstance

        return availableCost


    # ------------------------- END Availability Cost -------------------------

    # ------------------------- Load balancing the rest ---------------------

    def calculateLoadBalancingR(self, chromosome):
        totalServiceRate = 0
        UsedFogNumber = []
        FogsServiceRate = []
        FogsCPURest = {}
        FogsRAMRest = {}
        LoadBalanceDegree = 0
        UsedFog = False
        fogCPUMean = 0
        fogRAMMean = 0
        fogCPUTotal = 0
        fogRAMTotal = 0
        fogCPUDispo = 0
        fogRAMDispo = 0
        for fog in range(self.system.fogNumber):
            fogCPU = 0
            fogRAM = 0
            for ServiceId in range(self.system.serviceNumber):
                if chromosome[ServiceId][fog] == 1:
                    UsedFog = True
                    fogCPU = fogCPU + self.system.serviceResourcesCPU[ServiceId]
                    fogRAM = fogRAM + self.system.serviceResourcesRAM[ServiceId]
                    fogCPUTotal = fogCPUTotal + self.system.serviceResourcesCPU[ServiceId]
                    fogRAMTotal = fogRAMTotal + self.system.serviceResourcesRAM[ServiceId]
            if (UsedFog):
                UsedFogNumber.append(fog)
                FogsCPURest[fog] = self.system.nodeCPU[fog] - fogCPU
                FogsRAMRest[fog] = self.system.nodeRAM[fog] - fogRAM
            UsedFog = False
        UsedFogNumberLength = len(UsedFogNumber)

        for fog in UsedFogNumber:
            fogCPUDispo = fogCPUDispo + self.system.nodeCPU[fog]
            fogRAMDispo = fogRAMDispo + self.system.nodeRAM[fog]

        fogCPUMean = (fogCPUDispo - fogCPUTotal) / UsedFogNumberLength
        fogRAMMean = (fogRAMDispo - fogRAMTotal) / UsedFogNumberLength
        # print("UsedFogNumber",UsedFogNumber,"totalServiceRate",totalServiceRate,"FogsServiceRate",FogsServiceRate)
        for fogId in UsedFogNumber:
            LoadBalanceDegree = LoadBalanceDegree + 0.5 * pow(FogsCPURest[fogId] - fogCPUMean, 2) + 0.5 * pow(FogsRAMRest[fogId] - fogRAMMean, 2)
        return np.round(math.sqrt(LoadBalanceDegree / UsedFogNumberLength) / (0.5*sum(FogsCPURest)+0.5*sum(FogsRAMRest)),2)

    # ------------------------- Load balancing degree -------------------------

    def calculateLoadBalancing(self,chromosome):
        totalServiceRate = 0
        UsedFogNumber = []
        FogsServiceRate = []
        FogsCPUUtilisation = {}
        FogsRAMUtilisation = {}
        LoadBalanceDegree = 0
        UsedFog = False
        fogCPUMean = 0
        fogRAMMean = 0
        fogCPUTotal = 0
        fogRAMTotal = 0
        for fog in range(self.system.fogNumber):
            fogServiceRate = 0
            fogCPU = 0
            fogRAM = 0
            for ServiceId in range(self.system.serviceNumber):

                if chromosome[ServiceId][fog] == 1:
                    UsedFog = True
                    totalServiceRate = totalServiceRate + self.system.AppConfig[ServiceId]["resource"]["ServiceRate"]
                    fogServiceRate = fogServiceRate + self.system.AppConfig[ServiceId]["resource"]["ServiceRate"]
                    fogCPU = fogCPU + numReplicas * self.system.serviceResourcesCPU[ServiceId]
                    fogRAM = fogRAM + numReplicas * self.system.serviceResourcesRAM[ServiceId]
                    fogCPUTotal = fogCPUTotal + self.system.serviceResourcesCPU[ServiceId]
                    fogRAMTotal = fogRAMTotal + self.system.serviceResourcesRAM[ServiceId]

            FogsServiceRate.append(fogServiceRate)
            if(UsedFog):
                UsedFogNumber.append(fog)
                FogsCPUUtilisation[fog] = fogCPU
                FogsRAMUtilisation[fog] = fogRAM
            UsedFog = False
        UsedFogNumberLength = len(UsedFogNumber)

        ServiceRateMean = totalServiceRate / UsedFogNumberLength
        fogCPUMean = fogCPUTotal / UsedFogNumberLength
        fogRAMMean = fogRAMTotal / UsedFogNumberLength
        #print("UsedFogNumber",UsedFogNumber,"totalServiceRate",totalServiceRate,"FogsServiceRate",FogsServiceRate)
        for fogId in UsedFogNumber:
            LoadBalanceDegree = LoadBalanceDegree + 0.5*pow(FogsCPUUtilisation[fogId]-fogCPUMean,2) + 0.5*pow(FogsRAMUtilisation[fogId]-fogRAMMean,2)
        return np.round(math.sqrt(LoadBalanceDegree / UsedFogNumberLength),2)

    # ------------------------- END Load balancing degree -------------------------

    # ------------------------- Model constraints -------------------------

    def ServiceStability(self, chromosome):

        fogConsumRAM = 0
        fogConsumCPU = 0
        MSId = 0
        for fogId in range(0, self.system.fogNumber):
            for serviceId in range(0, self.serviceInstanceNumber):
                if chromosome[serviceId][fogId] == 1:
                    if (serviceId == self.system.arrayMaxNeededInstancesPositions[MSId+1]):
                        MSId = MSId +1
                    fogConsumRAM = fogConsumRAM + self.system.serviceResourcesRAM[MSId]
                    fogConsumCPU = fogConsumCPU + self.system.serviceResourcesCPU[MSId]
            if (fogConsumRAM > self.system.nodeRAM[fogId] and fogConsumCPU > self.system.nodeCPU[fogId]):
                return False
            fogConsumRAM = 0
            fogConsumCPU = 0

        return True

    def EnoughResourceFogDevice(self, chromosome):

        fogConsumRAM = 0
        fogConsumCPU = 0
        for fogId in range(0, self.system.fogNumber):
            for serviceId in range(0, self.system.serviceNumber):
                if chromosome[serviceId][fogId] == 1:
                    fogConsumRAM = fogConsumRAM + self.system.serviceResourcesRAM[serviceId]
                    fogConsumCPU = fogConsumCPU + self.system.serviceResourcesCPU[serviceId]
            if (fogConsumRAM > self.system.nodeRAM[fogId] and fogConsumCPU > self.system.nodeCPU[fogId]):
                return False
            fogConsumRAM = 0
            fogConsumCPU = 0

        return True

    def checkConstraints(self, chromosome):

        if (self.EnoughResourceFogDevice(chromosome)):
            return True
        return False

    # ------------------------- END Model constraints -------------------------

    #***************************************************************************************
    #-------------------------- Objectives and fitness calculation -------------------------
    #***************************************************************************************

    def calculateFitnessObjectivesD(self, chromosome,pop):
        chr_fitness = {}
        total = float('inf')
        # print("--------------------------------------")
        # print(chromosome)
        dealineViolation = float('inf')
        if self.checkConstraints(chromosome):
            dealineViolation = self.calculateDeadlineViolation(chromosome)
            #print("dealineViolation", dealineViolation)
            if (not isinstance(dealineViolation,bool)):
                chr_fitness["deadlineViolation"] = dealineViolation
                chr_fitness["totalResource"] = self.calculateTotalResourcesN(chromosome)
                chr_fitness["reliabilityInstance"] = self.calculateAvailability()
                chr_fitness["loadbalancing"] = self.calculateLoadBalancingR(chromosome)
                chr_fitness["total"] = total
            else:
                #print("NededInstanceConstaints")
                chr_fitness["deadlineViolation"] = float('inf')
                chr_fitness["totalResource"] = float('inf')
                chr_fitness["reliabilityInstance"] = float('inf')
                chr_fitness["loadbalancing"] = float('inf')
                chr_fitness["total"] = total
        else:
            #print("ResourcesConstraints")
            chr_fitness["deadlineViolation"] = float('inf')
            chr_fitness["totalResource"] = float('inf')
            chr_fitness["reliabilityInstance"] = float('inf')
            chr_fitness["loadbalancing"] = float('inf')
            chr_fitness["total"] = total


        return chr_fitness


    def calculatePopulationFitnessObjectives(self, pop): # second step fitness function calculation
        for index, individual in enumerate(pop.population):
            cit_fitness = self.calculateFitnessObjectivesD(individual,pop)
            cit_fitness["index"] = index
            pop.fitness[index] = cit_fitness

    # ***************************************************************************************
    # ------------------------ END Objectives and fitness calculation -----------------------
    # ***************************************************************************************


    # ------------------------- Population GENERATION -------------------------
    def generatePopulation(self, popT): # list chromosome position value
        for individual in range(self.populationSize):
            chromosome = [[0 for j in range(self.system.fogNumber)] for i in range(self.system.serviceNumber)]

            for MServiceId in range(self.system.serviceNumber):
                for i in range(self.system.UsersJ[MServiceId]["device"]["SumNeededInstances"]):
                    chromosome[MServiceId][self.rndPOP.randint(0,self.system.fogNumber-1)] = 1

            popT.population[individual] = chromosome
        self.calculatePopulationFitnessObjectives(popT)

    def generatePopulation3(self, popT): # matrix chromosome binary value

        self.serviceInstanceNumber = sum(self.system.arrayMaxNeededInstances)
        for individual in range(self.populationSize):
            chromosome = [[0 for j in range(self.system.fogNumber)] for i in range(self.serviceInstanceNumber)]

            for MServiceId in range(self.system.serviceNumber):
                for i in range(self.system.UsersJ[MServiceId]["device"]["SumNeededInstances"]):
                    chromosome[self.rndPOP.randint(self.system.arrayMaxNeededInstancesPositions[MServiceId],
                               self.system.arrayMaxNeededInstancesPositions[MServiceId + 1]-1)][self.rndPOP.randint(0,self.system.fogNumber-1)] = 1

            popT.population[individual] = chromosome

        self.calculatePopulationFitnessObjectives(popT)

    def generatePopulationTEST(self, popT):

        self.serviceInstanceNumber  = sum(self.system.arrayMaxNeededInstances)
        for individual in range(self.populationSize):
            chromosome = [[0 for j in range(self.system.fogNumber)] for i in range(self.serviceInstanceNumber)]

            chromosome[0][0] = 0
            chromosome[0][1] = 0
            chromosome[0][2] = 0
            chromosome[0][3] = 1
            chromosome[0][4] = 0
            chromosome[0][5] = 0
            chromosome[1][0] = 0
            chromosome[1][1] = 0
            chromosome[1][2] = 0
            chromosome[1][3] = 0
            chromosome[1][4] = 0
            chromosome[1][5] = 0
            chromosome[2][0] = 0
            chromosome[2][1] = 0
            chromosome[2][2] = 0
            chromosome[2][3] = 0
            chromosome[2][4] = 0
            chromosome[2][5] = 0


            popT.population[individual] = chromosome

        self.calculatePopulationFitnessObjectives(popT)

    # ------------------------- END Population GENERATION -------------------------

    # ------------------------- Father Selection -------------------------

    def tournamentSelection(self, k, popSize):
        selected = sys.maxsize
        for i in range(k):
            selected = min(selected, self.rndEVOL.randint(0, popSize - 1))
        return selected

    def fatherSelection(self, orderedFathers):  # TODO
        i = self.tournamentSelection(2, len(orderedFathers))
        return orderedFathers[i]["index"]

    def orderPopulation(self, popT):
        valuesToOrder = []
        for i, v in enumerate(popT.fitness):
            citizen = {}
            citizen["index"] = i
            citizen["fitness"] = v["total"]
            valuesToOrder.append(citizen)

        return sorted(valuesToOrder, key=lambda k: (k["fitness"]))



