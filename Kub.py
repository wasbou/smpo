import random
import SystemConfiguration
import MyConfig
import NSGA2 as nsga2
import NetworkPlot
import sys
import json
import copy
import math as math
import numpy as np
class Kub:

    def __init__(self, system, conf_):

        self.system = system
        self.conf = conf_
        self.totalSolutionDealine = {}
        self.totalSolution = {}


    def scheduling(self):
        self.solution = [[] for i in range(self.system.serviceNumber)]
        self.solutionFog = [[] for i in range(self.system.fogNumber)]

        self.TotalAvailableInstancedict = {}

        #sorted based on deadline predicated
        self.services =  copy.deepcopy(self.system.AppConfig)
        self.services.sort(key=lambda x: x["resource"]["Deadline"]) # services sorted based on deadlines
        #print("sorted services",self.services)
        fogCPU = copy.deepcopy(self.system.nodeCPU)
        fogRAM = copy.deepcopy(self.system.nodeRAM)
        serviceCPU = copy.deepcopy(self.system.serviceResourcesCPU)
        serviceRAM = copy.deepcopy(self.system.serviceResourcesRAM)
        CheckDealineFirst = True
        TotalServiceDelay = 0
        self.deadlineUserViolation = 0
        self.UserViolation = 0


        self.deadlineviolation = 0
        self.TotalNeededInstances = 0
        LeastRequestedPriority = {}
        for service in self.services:
            #print('###############################################################')
            serviceId = service["serviceId"]
            serviceRate = self.system.AppConfig[serviceId]["resource"]["ServiceRate"]
            deadline = self.system.AppConfig[serviceId]["resource"]["Deadline"]
            self.TotalAvailableInstance = []

            UserMs = self.system.UsersJ[serviceId]
            DataIn = UserMs["device"]["Data_in"]
            for user in range(len(UserMs["device"]["gatewayId"])):
                # 1 CKECK THE resource constraints
                chosenFog = list()
                for fogId in range(0, self.system.fogNumber):
                    # Not enough resource check
                    LeastRequestedPriority[fogId] = (fogCPU[fogId] / (2 * self.system.nodeCPU[fogId])) + (fogRAM[fogId] / (2 * self.system.nodeRAM[fogId]))  # LeastRequestedPriority
                    if fogRAM[fogId] >= serviceRAM[serviceId] and fogCPU[fogId] >= serviceCPU[serviceId]:
                        chosenFog.append(fogId)
                #print("chosenFog", chosenFog)

                # 1 CKECK for deadline
                delayMinPriority = {}
                UserServiceDelay = 0
                UserNetworkDelay = 0
                TotalAvailableInstanceUser = 0
                deadlinecheck = 0
                userGateway = UserMs["device"]["gatewayId"][user]
                #print('---------------------- User '+str(userGateway)+' service '+str(serviceId)+'---------------------')
                UserNeededInstance = UserMs["device"]["NeededInstance"][user]  # user needed Instance
                self.TotalNeededInstances = self.TotalNeededInstances + UserNeededInstance

                UserAccessDelay = UserMs["device"]["AccessDelay"][user]  # Access delay
                UserPocessingDelay = 1 / (serviceRate - (UserMs["device"]["Requestrate"][user] / UserNeededInstance))  # processing delay

                fog_service_dict = {k: v for k, v in sorted(self.system.allShortestPathLength[userGateway].items(), key=lambda x: x[1])}
                fog_shortest_path_list = list(fog_service_dict.keys())
                UserShortestPath = self.system.allShortestPath[userGateway]  # the path [f1,f2..]
                DealineListFogFirst = []
                index = 0  # index begin with the shortest path first
                # print("fog_shortest_path_list",fog_shortest_path_list)
                # print("chosenFog",chosenFog)

                # sotred the chosen one based on shortest path
                for fog in fog_shortest_path_list:
                    if fog in chosenFog:
                        DealineListFogFirst.append(fog)
                # print("fog_service_dict",fog_service_dict)
                # print("DealineListFogFirst",DealineListFogFirst)
                for fog_node in DealineListFogFirst:
                    n_node = len(UserShortestPath[fog_node]) - 1
                    UserNetworkDelay = fog_service_dict[fog_node] * DataIn + 2 * n_node
                    UserServiceDelay = UserPocessingDelay + UserNetworkDelay + UserAccessDelay

                    if UserServiceDelay <= deadline:
                        delayMinPriority[fog_node] = 0

                    else:
                        delayMinPriority[fog_node] = UserServiceDelay-deadline


                #print("delayMinPriority",delayMinPriority)
                priority = {}
                #print("LeastRequestedPriority",LeastRequestedPriority)
                #self.TotalAvailableInstance.append(UserMs["device"]["NeededInstance"][user] - UserNeededInstance)
                #print("service",serviceId,"user",user,"nedded",UserMs["device"]["NeededInstance"][user], "rest",UserNeededInstance)

                for fog in DealineListFogFirst:
                    priority[fog] = 0.5*LeastRequestedPriority[fog] + 0.5*delayMinPriority[fog]

                priority_sort = [k for k,v in sorted(priority.items(), key=lambda x: x[1])]
                #print("priority_sort",priority_sort)

                fogI = 0
                UserNeededInstance = UserMs["device"]["NeededInstance"][user]
                if(len(priority_sort) != 0):
                    while UserNeededInstance != 0:
                        node = priority_sort[fogI]
                        if fogRAM[node] >= serviceRAM[serviceId] and fogCPU[node] >= serviceCPU[serviceId]:
                            #TotalServiceDelay = TotalServiceDelay + UserServiceDelay
                            fogRAM[node] = fogRAM[node] - serviceRAM[serviceId]
                            fogCPU[node] = fogCPU[node] - serviceCPU[serviceId]
                            UserNeededInstance = UserNeededInstance - 1
                            self.deadlineUserViolation = self.deadlineUserViolation + delayMinPriority[node]

                            self.solution[serviceId].append(node)
                            self.solutionFog[node].append(serviceId)
                        else:
                            fogI = fogI + 1
                        if (fogI == len(priority_sort)):
                            break
                else:
                    self.deadlineUserViolation = self.deadlineUserViolation + UserNeededInstance * deadline
                    self.UserViolation = self.UserViolation + 1

            self.TotalAvailableInstancedict[serviceId] = self.TotalAvailableInstance

        self.totalSolutionDealine["placement"] = self.solution
        self.totalSolutionDealine["deadlineViolation"] = np.round(self.deadlineUserViolation,2)
        self.totalSolutionDealine["totalResource"] = self.calculateTotalResources(self.solutionFog)
        self.totalSolutionDealine["loadBalancing"]  = self.calculateLoadBalancing(self.solutionFog)
        self.totalSolutionDealine["userViolation"] = self.UserViolation

        print("solution",self.totalSolutionDealine)
        print("self.UserViolation",self.UserViolation)
        return self.totalSolutionDealine

    def scheduling2(self):
        self.solution = [[] for i in range(self.system.serviceNumber)]
        self.solutionFog = [[] for i in range(self.system.fogNumber)]

        self.TotalAvailableInstancedict = {}

        # sorted based on deadline predicated
        self.services = copy.deepcopy(self.system.AppConfig)

        #self.services.sort(key=lambda x: x["resource"]["Deadline"])  # services sorted based on deadlines

        # print("sorted services",self.services)
        fogCPU = copy.deepcopy(self.system.nodeCPU)
        fogRAM = copy.deepcopy(self.system.nodeRAM)
        serviceCPU = copy.deepcopy(self.system.serviceResourcesCPU)
        serviceRAM = copy.deepcopy(self.system.serviceResourcesRAM)
        CheckDealineFirst = True
        TotalServiceDelay = 0
        self.deadlineUserViolation = 0
        self.UserViolation = 0

        self.deadlineviolation = 0
        self.TotalNeededInstances = 0
        LeastRequestedPriority = {}
        for service in self.services:
            #print('###############################################################')
            serviceId = service["serviceId"]
            serviceRate = self.system.AppConfig[serviceId]["resource"]["ServiceRate"]
            deadline = self.system.AppConfig[serviceId]["resource"]["Deadline"]
            self.TotalAvailableInstance = []

            UserMs = self.system.UsersJ[serviceId]
            DataIn = UserMs["device"]["Data_in"]
            for user in range(len(UserMs["device"]["gatewayId"])):
                # 1 CKECK THE resource constraints
                chosenFog = list()
                for fogId in range(0, self.system.fogNumber):
                    # Not enough resource check
                    LeastRequestedPriority[fogId] = (fogCPU[fogId] / (2 * self.system.nodeCPU[fogId])) + (
                                fogRAM[fogId] / (2 * self.system.nodeRAM[fogId]))  # LeastRequestedPriority
                    if fogRAM[fogId] >= serviceRAM[serviceId] and fogCPU[fogId] >= serviceCPU[serviceId]:
                        chosenFog.append(fogId)
                #print("chosenFog", chosenFog)

                # 1 CKECK for deadline
                delayMinPriority = {}
                UserServiceDelay = 0
                UserNetworkDelay = 0
                TotalAvailableInstanceUser = 0
                deadlinecheck = 0
                userGateway = UserMs["device"]["gatewayId"][user]
                #print('---------------------- User ' + str(userGateway) + ' service ' + str(serviceId) + '---------------------')
                UserNeededInstance = UserMs["device"]["NeededInstance"][user]  # user needed Instance
                self.TotalNeededInstances = self.TotalNeededInstances + UserNeededInstance

                UserAccessDelay = UserMs["device"]["AccessDelay"][user]  # Access delay
                UserPocessingDelay = 1 / (serviceRate - (UserMs["device"]["Requestrate"][user] / UserNeededInstance))  # processing delay

                fog_service_dict = {k: v for k, v in
                                    sorted(self.system.allShortestPathLength[userGateway].items(), key=lambda x: x[1])}
                fog_shortest_path_list = list(fog_service_dict.keys())
                UserShortestPath = self.system.allShortestPath[userGateway]  # the path [f1,f2..]
                DealineListFogFirst = []
                index = 0  # index begin with the shortest path first
                # print("fog_shortest_path_list",fog_shortest_path_list)
                # print("chosenFog",chosenFog)

                # sotred the chosen one based on shortest path
                for fog in fog_shortest_path_list:
                    if fog in chosenFog:
                        DealineListFogFirst.append(fog)
                # print("fog_service_dict",fog_service_dict)
                # print("DealineListFogFirst",DealineListFogFirst)
                for fog_node in DealineListFogFirst:
                    n_node = len(UserShortestPath[fog_node]) - 1
                    UserNetworkDelay = fog_service_dict[fog_node] * DataIn + 2 * n_node
                    UserServiceDelay = UserPocessingDelay + UserNetworkDelay + UserAccessDelay

                    if UserServiceDelay <= deadline:
                        delayMinPriority[fog_node] = 0

                    else:
                        delayMinPriority[fog_node] = UserServiceDelay - deadline

                # print("delayMinPriority",delayMinPriority)
                priority = {}
                # print("LeastRequestedPriority",LeastRequestedPriority)
                # self.TotalAvailableInstance.append(UserMs["device"]["NeededInstance"][user] - UserNeededInstance)
                # print("service",serviceId,"user",user,"nedded",UserMs["device"]["NeededInstance"][user], "rest",UserNeededInstance)

                for fog in DealineListFogFirst:
                    priority[fog] = 0.5 * LeastRequestedPriority[fog]

                priority_sort = [k for k, v in sorted(priority.items(), key=lambda x: x[1])]
                #print("priority_sort", priority_sort)

                fogI = 0
                UserNeededInstance = UserMs["device"]["NeededInstance"][user]
                if (len(priority_sort) != 0):
                    while UserNeededInstance != 0:
                        node = priority_sort[fogI]
                        if fogRAM[node] >= serviceRAM[serviceId] and fogCPU[node] >= serviceCPU[serviceId]:
                            # TotalServiceDelay = TotalServiceDelay + UserServiceDelay
                            fogRAM[node] = fogRAM[node] - serviceRAM[serviceId]
                            fogCPU[node] = fogCPU[node] - serviceCPU[serviceId]
                            UserNeededInstance = UserNeededInstance - 1
                            self.deadlineUserViolation = self.deadlineUserViolation + delayMinPriority[node]

                            self.solution[serviceId].append(node)
                            self.solutionFog[node].append(serviceId)
                        else:
                            fogI = fogI + 1
                        if (fogI == len(priority_sort)):
                            break
                else:
                    self.deadlineUserViolation = self.deadlineUserViolation + UserNeededInstance * deadline
                    self.UserViolation = self.UserViolation + 1

            self.TotalAvailableInstancedict[serviceId] = self.TotalAvailableInstance

        self.totalSolution["placement"] = self.solution
        self.totalSolution["deadlineViolation"] = np.round(self.deadlineUserViolation,2)
        self.totalSolution["totalResource"] = self.calculateTotalResources(self.solution)
        self.totalSolution["loadBalancing"] = self.calculateLoadBalancing(self.solutionFog)
        self.totalSolution["userViolation"] = self.UserViolation

        print("solution", self.totalSolution)
        print("self.UserViolation", self.UserViolation)

        return self.totalSolution

    def calculateTotalResources(self,Solution):
        FogsCPUUtilisation = {}
        FogsRAMUtilisation = {}
        UsedFogNumber = [index for index, value in enumerate(Solution) if len(value) != 0]
        UsedFogNumberLength = len(UsedFogNumber)
        TotalResource = 0
        for fog in range(self.system.fogNumber):
            fogCPU = 0
            fogRAM = 0
            for ServiceId in Solution[fog]:
                fogCPU = fogCPU + self.system.serviceResourcesCPU[ServiceId]
                fogRAM = fogRAM + self.system.serviceResourcesRAM[ServiceId]
            FogsCPUUtilisation[fog] = fogCPU / self.system.nodeCPU[fog]
            FogsRAMUtilisation[fog] = fogRAM / self.system.nodeRAM[fog]
        for fogId in UsedFogNumber:
            TotalResource = TotalResource + 0.5*FogsCPUUtilisation[fogId] + 0.5*FogsRAMUtilisation[fogId]
        return np.round(TotalResource / UsedFogNumberLength,2)


    def calculateAvailability(self):
        availableCost = 0
        print("TOTAL",self.TotalAvailableInstancedict)
        for UserMs in self.system.UsersJ: # for each user in the system
            ServiceId = UserMs["device"]["serviceId"] # get the serviceId requested by the user
            for user in range(len(UserMs["device"]["gatewayId"])): # users requesting the serviceId
                UserNeededInstance = UserMs["device"]["NeededInstance"][user]
                # print("user",user,"UserNeededInstance",UserNeededInstance)
                # print("user",user,"service",ServiceId,self.TotalAvailableInstancedict[ServiceId][user])
                if (self.TotalAvailableInstancedict[ServiceId][user] != 0 ):
                    availableCost = availableCost + (UserNeededInstance/self.TotalAvailableInstancedict[ServiceId][user])
                else:
                    availableCost = availableCost + UserNeededInstance

        return availableCost

    def calculateLoadBalancing(self,solutionFog):
        totalServiceRate = 0
        UsedFogNumber = [index for index,value in enumerate(solutionFog) if len(value) != 0 ]
        UsedFogNumberLength = len(UsedFogNumber)
        LoadBalanceDegree = 0
        fogCPUTotal = 0
        fogRAMTotal = 0
        FogsCPURest = {}
        FogsRAMRest = {}
        fogCPUMean = 0
        fogRAMMean = 0
        fogRAMDispo = 0
        fogCPUDispo = 0
        for fog in range(self.system.fogNumber):
            fogCPU = 0
            fogRAM = 0
            for serviceId in solutionFog[fog]:
                fogCPUTotal = fogCPUTotal + self.system.serviceResourcesCPU[serviceId]
                fogRAMTotal = fogRAMTotal + self.system.serviceResourcesRAM[serviceId]
                fogCPU = fogCPU + self.system.serviceResourcesCPU[serviceId]
                fogRAM = fogRAM + self.system.serviceResourcesRAM[serviceId]

            FogsCPURest[fog] = self.system.nodeCPU[fog] - fogCPU
            FogsRAMRest[fog] = self.system.nodeRAM[fog] - fogRAM

        for fog in UsedFogNumber:
            fogCPUDispo = fogCPUDispo + self.system.nodeCPU[fog]
            fogRAMDispo = fogRAMDispo + self.system.nodeRAM[fog]

        fogCPUMean = (fogCPUDispo - fogCPUTotal) / UsedFogNumberLength
        fogRAMMean = (fogRAMDispo - fogRAMTotal) / UsedFogNumberLength


        for fogId in UsedFogNumber:
            LoadBalanceDegree = LoadBalanceDegree + 0.5*pow(FogsCPURest[fogId]-fogCPUMean,2) + 0.5*pow(FogsCPURest[fogId]-fogRAMMean,2)
        return np.round(math.sqrt(LoadBalanceDegree / UsedFogNumberLength) / (0.5*sum(FogsCPURest)+0.5*sum(FogsRAMRest)),2)