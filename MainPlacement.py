
import random
import SystemConfiguration
import MyConfig
from NSGA2C import NSGA2v1 as nsga2v1
from NSGA2v2 import NSGA2v2 as nsga2v2
import NSGA2 as nsga2
import NetworkPlot
import sys
import json
import Kub
import pickle

"""
# Expriment
ServiceScaleLevel = [20,40,60,80]
UserScaleLevel = [25,50,100,200]
performance = [200,80]

experimentList = []
# 3 - mutation probability 0-1.0
# 4 - population size
# 5 - seed for random values for generation of the initial population
# 6 - seed for random values for evolution of populations
experimentList.append([0.25,100,400,888])
experimentList.append([0.25,100,500,444])
experimentList.append([0.20,100,300,888])
experimentList.append([0.20,100,500,888])

"""

file_path = "Result"
agregatedResults = open(file_path+'/agregatedResults.csv', 'w')

resultValues = "ex_number;fog;ms;user;deadline;resource;LB;MinD;MinR,MinLB;population;generation;POPseed;EVOLseed;"
agregatedResults.write(resultValues+'\n')
agregatedResults.flush()


#****************************************************************************************************
#inizializations and set up
#****************************************************************************************************


cnf_ = MyConfig.myConfig(100,500,800,0.25,100) # Population and generation configuration

system = SystemConfiguration.SystemConfiguration(cnf_,30) # serviceNumber 2, maxuserPerGateway 3


system.networkGeneration() # Fog infrastructure
#system.GraphPlot() # plot fog
system.serviceGeneration() # microservice application
system.userGeneration(11) # users placement


# --------------------------------------------------------------------------
# Kubernetes solution
#---------------------------------------------------------------------------

# kubernetes = Kub.Kub(system,cnf_)
# kubernetes.scheduling()
# print("######################################")
# kubernetes.scheduling2()
# print("######################################")


# --------------------------------------------------------------------------
# NSGA2 solution
#---------------------------------------------------------------------------


g = nsga2.NSGA2(system,cnf_)


# generationSolution = list()
# generationPareto = list()

# print(f"Population Zero {g.corega.populationPt.population}")
# print(f"Fitness Zero {g.corega.populationPt.fitness}")

generation = 100
# Get the min population fitness
# minV = float('inf')
# minIdx = -1

# --------------------------------------------------------------------------
# currentSolution = {}
# currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]
# currentSolution['population'] = g.corega.populationPt.population[minIdx]
# generationSolution.append(currentSolution)
#
# NDeadline = 1 / 3
# NDelay = 1 / 3
# NRessource = 1 / 3
# NLoadBalancing = 1 / 3
#
#
# for i in range(0, generation):
#     g.evolve2()
#     minV = float('inf')
#     minIdx = -1
#     deadlineViolationN = 0
#     totalResourceN = 0
#     reliabilityInstanceN = 0
#     TotalObjective = 0
#     loadbalancingN = 0
#     for idx, v in enumerate(g.corega.populationPt.fitness):
#         if (g.corega.populationPt.maxDeadlineViolation != g.corega.populationPt.minDeadlineViolation):
#             deadlineViolationN = NDeadline * (v["deadlineViolation"] - g.corega.populationPt.minDeadlineViolation) / (g.corega.populationPt.maxDeadlineViolation - g.corega.populationPt.minDeadlineViolation)
#         else:
#             deadlineViolationN = v["deadlineViolation"]
#
#         if (g.corega.populationPt.maxRessource != g.corega.populationPt.minRessource):
#             totalResourceN = NRessource * (v["totalResource"] - g.corega.populationPt.minRessource) / (g.corega.populationPt.maxRessource - g.corega.populationPt.minRessource)
#         else:
#             totalResourceN = v["totalResource"]
#
#         if (g.corega.populationPt.maxloadBalancing != g.corega.populationPt.minloadBalancing):
#             loadbalancingN = NLoadBalancing * (v["loadbalancing"] - g.corega.populationPt.minloadBalancing) / (g.corega.populationPt.maxloadBalancing - g.corega.populationPt.minloadBalancing)
#         else:
#             loadbalancingN = v["loadbalancing"]
#
#         TotalObjective = deadlineViolationN * 1 / 3 + totalResourceN * 1 / 3 + loadbalancingN * 1 / 3
#         g.corega.populationPt.fitness[idx]['total'] = TotalObjective
#
#         if TotalObjective < minV:
#             minIdx = idx
#             minV = TotalObjective
#     currentSolution['population'] = g.corega.populationPt.population[minIdx]
#     currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]
#
#     print("----------------------------------------------------")
#     # print(f"Population  {g.corega.populationPt.population}")
#     # print(f"Fitness  {g.corega.populationPt.fitness}")
#     print(f"Objective  {g.corega.populationPt}")
#     print("----------------------------------------------------")
#
#     generationSolution.append(currentSolution)
#
# # Sort the solution
# generationSolution.sort(key=lambda x: x["fitness"]["total"])
# print("all solution", generationSolution)
#
#
#
# FinalResult = generationSolution[0]["fitness"]
# resultValues = str(1) + ";" + str("NSGA2v1") + ";" + str(20) + ";" + str(30) + ";" + str(11) + ";" + str(FinalResult["deadlineViolation"]) + ";" + str(FinalResult["totalResource"]) + ";" + str(FinalResult["loadbalancing"]) + ";" + str(g.corega.populationPt.minDeadlineViolation) + ";" + str(
#     g.corega.populationPt.minRessource) + ";" + str(g.corega.populationPt.minloadBalancing) + ";" + str(100) + ";" + str("0") + ";" + str(system.UsersNumber) + ";" + str(
#     system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) + ";" + str(system.neededinstanceSum[1]) + ";" + str(system.neededinstanceSum[2]) + ";" + str(system.neededinstanceSum[3]) + ";" + str(system.neededinstanceSum[4])
# agregatedResults.write(resultValues + '\n')
# agregatedResults.flush()

# # ------------------------------crowdsourcing----------------------------
# 
# 
g3 = nsga2.NSGA2(system,cnf_)

NDeadline = 1 / 3
NDelay = 1 / 3
NRessource = 1 / 3
NLoadBalancing = 1 / 3

generationSolution = list()
generationPareto = list()

# print(f"Population Zero {g.corega.populationPt.population}")
# print(f"Fitness Zero {g.corega.populationPt.fitness}")

minV = float('inf')
minIdx = -1

# --------------------------------------------------------------------------
currentSolution = {}
currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]
currentSolution['population'] = g.corega.populationPt.population[minIdx]
generationSolution.append(currentSolution)


for i in range(0, generation):
    g.evolve3()
    minV = float('inf')
    minIdx = -1
    deadlineViolationN = 0
    totalResourceN = 0
    reliabilityInstanceN = 0
    TotalObjective = 0
    loadbalancingN = 0
    for idx, v in enumerate(g.corega.populationPt.fitness):
        if (g.corega.populationPt.maxDeadlineViolation != g.corega.populationPt.minDeadlineViolation):
            deadlineViolationN = NDeadline * (
                    v["deadlineViolation"] - g.corega.populationPt.minDeadlineViolation) / (
                                         g.corega.populationPt.maxDeadlineViolation - g.corega.populationPt.minDeadlineViolation)
        else:
            deadlineViolationN = v["deadlineViolation"]

        if (g.corega.populationPt.maxRessource != g.corega.populationPt.minRessource):
            totalResourceN = NRessource * (v["totalResource"] - g.corega.populationPt.minRessource) / (
                    g.corega.populationPt.maxRessource - g.corega.populationPt.minRessource)
        else:
            totalResourceN = v["totalResource"]

        if (g.corega.populationPt.maxAvailability != g.corega.populationPt.minAvailability):
            reliabilityInstanceN = NAvailability * (
                    v["reliabilityInstance"] - g.corega.populationPt.minAvailability) / (
                                           g.corega.populationPt.maxAvailability - g.corega.populationPt.minAvailability)
        else:
            reliabilityInstanceN = v["reliabilityInstance"]

        if (g.corega.populationPt.maxloadBalancing != g.corega.populationPt.minloadBalancing):
            loadbalancingN = NLoadBalancing * (
                    v["loadbalancing"] - g.corega.populationPt.minloadBalancing) / (
                                     g.corega.populationPt.maxloadBalancing - g.corega.populationPt.minloadBalancing)
        else:
            loadbalancingN = v["loadbalancing"]

        TotalObjective = deadlineViolationN * 1 / 3 + totalResourceN * 1 / 3 + loadbalancingN * 1 / 3
        g.corega.populationPt.fitness[idx]['total'] = TotalObjective

        if TotalObjective < minV:
            minIdx = idx
            minV = TotalObjective
    currentSolution['population'] = g.corega.populationPt.population[minIdx]
    currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]

    print("----------------------------------------------------")
    # print(f"Population  {g.corega.populationPt.population}")
    # print(f"Fitness  {g.corega.populationPt.fitness}")
    print(f"Objective  {g.corega.populationPt}")
    print("----------------------------------------------------")

    generationSolution.append(currentSolution)

# Sort the solution
generationSolution.sort(key=lambda x: x["fitness"]["total"])
print("all solution", generationSolution)

FinalResult = generationSolution[0]["fitness"]
resultValues = str(1) + ";" + str("NSGA2v2") + ";" + str(20) + ";" + str(30) + ";" + str(11) + ";" + str(FinalResult["deadlineViolation"]) + ";" + str(FinalResult["totalResource"]) + ";" + str(FinalResult["loadbalancing"]) + ";" + str(g.corega.populationPt.minDeadlineViolation) + ";" + str(
    g.corega.populationPt.minRessource) + ";" + str(g.corega.populationPt.minloadBalancing) + ";" + str(100) + ";" + str("0") + ";" + str(system.UsersNumber) + ";" + str(
    system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) + ";" + str(system.neededinstanceSum[1]) + ";" + str(system.neededinstanceSum[2]) + ";" + str(system.neededinstanceSum[3]) + ";" + str(system.neededinstanceSum[4])
agregatedResults.write(resultValues + '\n')
agregatedResults.flush()


"""
#--------------------- Plot result ------------------------------------
FinalResult = generationSolution[0]["fitness"]
# SolutionPlot = NetworkPlot.NetworkPlot(system,cnf_, generationSolution,{})
# SolutionPlot.plotNSGA2()

# export then result to csv

resultValues = str(1)+";"+str("NSGA2")+";"+str(4)+";"+str(2)+";"+str(FinalResult["deadlineViolation"])+";"+str(FinalResult["totalResource"] )+";"+str(round(FinalResult["loadbalancing"]))
agregatedResults.write(resultValues.replace(".", ",") + '\n')
agregatedResults.flush()

output = open(file_path + '/' + "firstTry" + '-lastGeneration.pkl', 'wb')
pickle.dump(g.corega.populationPt, output)
output.close()

agregatedResults.close()
"""

