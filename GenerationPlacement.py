import random
import SystemConfiguration
import MyConfig
import NSGA2 as nsga2
from NSGA2C import NSGA2v1 as nsga2v1
from NSGA2v2 import NSGA2v2 as nsga2v2
import NetworkPlot
import pickle
import sys
import json
from numba import jit, cuda
import Kub
import numpy as np
from datetime import datetime

experimentList = []
# 3 - mutation probability 0-1.0
# 4 - population size
# 5 - seed for random values for generation of the initial population
# 6 - seed for random values for evolution of populations
experimentList.append([100, 500, 888, 0.25])

ServiceScaleLevel = [10,30]
UserScaleLevel = [4,9,15]
# UserScaleLevel = [11,12,13,14,15,17,20]
# UserScaleLevel = [21,25]

performance = [80,8]
kubSolution = {}

generationList = [150]
file_path = "Result"
agregatedResults = open(file_path + '/Test/agregatedGeneration.csv', 'w')

resultValues = "ex;algo;fog;ms;user;deadline;resource;LB;MinD;MinR;MinLB;generation;UserViolation;numberOfUsers;maxNeededInstance;requestRate;1;2;3;4"
agregatedResults.write(resultValues.replace(".", ",") + '\n')
agregatedResults.flush()

i = 0
for ex in experimentList:
    i = i + 1
    for service in ServiceScaleLevel:
        # ****************************************************************************************************
        # inizializations and set up
        # ****************************************************************************************************

        cnf_ = MyConfig.myConfig(ex[0], ex[1], ex[2], ex[3], 20)  # Population and generation configuration
        system = SystemConfiguration.SystemConfiguration(cnf_, service)  # serviceNumber 2, maxuserPerGateway 3
        system.networkGeneration()  # Fog infrastructure
        # system.GraphPlot() # plot fog
        system.serviceGeneration()  # microservice application

        for user in UserScaleLevel:
            # experimentName = "ex- " + str(i) + " G- "
            # print("***********************************************************")
            # print("*" + experimentName + " :::: " + str(datetime.now()))
            # print("***********************************************************")

            # ****************************************************************************************************
            # Users setup
            # ****************************************************************************************************
            system.userGeneration(user)  # users placement

            # --------------------------------------------------------------------------
            # Kubernetes solution
            # ---------------------------------------------------------------------------

            kubernetes = Kub.Kub(system, cnf_)

            kubResult = kubernetes.scheduling()
            resultValues = str(i) + ";" + str("KubDealine") + ";" + str(20) + ";" + str(service) + ";" + str(
                user) + ";" + str(kubResult["deadlineViolation"]) + ";" + str(
                kubResult["totalResource"]) + ";" + str(kubResult["loadBalancing"]) + ";" + str(
                kubResult["deadlineViolation"]) + ";" + str(
                kubResult["totalResource"]) + ";" + str(kubResult["loadBalancing"]) + ";" + str(
                "200") + ";" + str(kubResult["userViolation"]) + ";" + str(system.UsersNumber) + ";" + str(system.MaxNeededInstance) + ";" + str(system.reqestRateTotal)+";" + str(system.neededinstanceSum[1])+ ";" + str(system.neededinstanceSum[2])+ ";" + str(system.neededinstanceSum[3])+ ";" + str(system.neededinstanceSum[4])
            agregatedResults.write(resultValues + '\n')
            agregatedResults.flush()

            # kubResult = kubernetes.scheduling2()
            # resultValues = str(i) + ";" + str("Kub") + ";" + str(20) + ";" + str(service) + ";" + str(
            #     user) + ";" + str(kubResult["deadlineViolation"]) + ";" + str(
            #     kubResult["totalResource"]) + ";" + str(round(kubResult["loadBalancing"])) + ";" + str(
            #     kubResult["deadlineViolation"]) + ";" + str(
            #     kubResult["totalResource"]) + ";" + str(round(kubResult["loadBalancing"])) + ";" + str(
            #     "200") + ";" + str(round(kubResult["userViolation"]))+ ";" + str(system.UsersNumber) + ";" + str(system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) +";" + str(system.neededinstanceSum[1])+ ";" + str(system.neededinstanceSum[2])+ ";" + str(system.neededinstanceSum[3])+ ";" + str(system.neededinstanceSum[4])
            # agregatedResults.write(resultValues.replace(".", ",") + '\n')
            # agregatedResults.flush()


            for generation in generationList:


                # --------------------------------------------------------------------------
                # NSGA2 solution
                # ---------------------------------------------------------------------------

                g = nsga2.NSGA2(system, cnf_)
                g2 = nsga2.NSGA2(system, cnf_)
                gv1 = nsga2v1.NSGA2(system, cnf_)
                gv2 = nsga2v2.NSGA2(system, cnf_)




                # ------------------------------NSGA-2----------------------------

                generationSolution = list()
                generationPareto = list()

                # Get the min population fitness
                minV = float('inf')
                minIdx = -1

                # --------------------------------------------------------------------------
                currentSolution = {}
                currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]
                currentSolution['population'] = g.corega.populationPt.population[minIdx]
                generationSolution.append(currentSolution)

                NDeadline = 1 / 3
                NDelay = 1 / 3
                NRessource = 1 / 3
                NAvailability = 0
                NLoadBalancing = 1 / 3


                for i in range(0, generation):
                    g.evolve2()
                    minV = float('inf')
                    minIdx = -1
                    deadlineViolationN = 0
                    totalResourceN = 0
                    reliabilityInstanceN = 0
                    TotalObjective = 0
                    loadbalancingN = 0
                    for idx, v in enumerate(g.corega.populationPt.fitness):
                        if (g.corega.populationPt.maxDeadlineViolation != g.corega.populationPt.minDeadlineViolation):
                            deadlineViolationN = NDeadline * (
                                    v["deadlineViolation"] - g.corega.populationPt.minDeadlineViolation) / (
                                                         g.corega.populationPt.maxDeadlineViolation - g.corega.populationPt.minDeadlineViolation)
                        else:
                            deadlineViolationN = v["deadlineViolation"]

                        if (g.corega.populationPt.maxRessource != g.corega.populationPt.minRessource):
                            totalResourceN = NRessource * (v["totalResource"] - g.corega.populationPt.minRessource) / (
                                    g.corega.populationPt.maxRessource - g.corega.populationPt.minRessource)
                        else:
                            totalResourceN = v["totalResource"]

                        if (g.corega.populationPt.maxAvailability != g.corega.populationPt.minAvailability):
                            reliabilityInstanceN = NAvailability * (
                                    v["reliabilityInstance"] - g.corega.populationPt.minAvailability) / (
                                                           g.corega.populationPt.maxAvailability - g.corega.populationPt.minAvailability)
                        else:
                            reliabilityInstanceN = v["reliabilityInstance"]

                        if (g.corega.populationPt.maxloadBalancing != g.corega.populationPt.minloadBalancing):
                            loadbalancingN = NLoadBalancing * (
                                    v["loadbalancing"] - g.corega.populationPt.minloadBalancing) / (
                                                     g.corega.populationPt.maxloadBalancing - g.corega.populationPt.minloadBalancing)
                        else:
                            loadbalancingN = v["loadbalancing"]

                        TotalObjective = deadlineViolationN * 1 / 3 + totalResourceN * 1 / 3 + loadbalancingN * 1 / 3
                        g.corega.populationPt.fitness[idx]['total'] = TotalObjective

                        if TotalObjective < minV:
                            minIdx = idx
                            minV = TotalObjective
                    currentSolution['population'] = g.corega.populationPt.population[minIdx]
                    currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]

                    print("----------------------------------------------------")
                    # print(f"Population  {g.corega.populationPt.population}")
                    # print(f"Fitness  {g.corega.populationPt.fitness}")
                    print(f"Objective  {g.corega.populationPt}")
                    print("----------------------------------------------------")

                    generationSolution.append(currentSolution)
                    FinalResult = currentSolution['fitness']
                    resultValues = str(i) + ";" + str("NSGA2") + ";" + str(20) + ";" + str(service) + ";" + str(
                        user) + ";" + str(FinalResult["deadlineViolation"]) + ";" + str(
                        FinalResult["totalResource"]) + ";" + str(FinalResult["loadbalancing"]) + ";" + str(
                        g.corega.populationPt.minDeadlineViolation) + ";" + str(
                        g.corega.populationPt.minRessource) + ";" + str(
                        g.corega.populationPt.minloadBalancing) + ";" + str(
                        i) + ";" + str("0") + ";" + str(system.UsersNumber) + ";" + str(
                        system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) + ";" + str(
                        system.neededinstanceSum[1]) + ";" + str(system.neededinstanceSum[2]) + ";" + str(
                        system.neededinstanceSum[3]) + ";" + str(system.neededinstanceSum[4])
                    agregatedResults.write(resultValues + '\n')
                    agregatedResults.flush()

                # Sort the solution
                generationSolution.sort(key=lambda x: x["fitness"]["total"])
                print("all solution", generationSolution)



                """ 
                ################################################################
                # ---------------------------NSGA-2V1---------------------------
                ################################################################
                """
                generationSolution = list()
                generationPareto = list()

                # Get the min population fitness
                minV = float('inf')
                minIdx = -1

                # --------------------------------------------------------------------------
                currentSolution = {}
                currentSolution['fitness'] = gv1.corega.populationPt.fitness[minIdx]
                currentSolution['population'] = gv1.corega.populationPt.population[minIdx]
                generationSolution.append(currentSolution)

                NDeadline = 1 / 3
                NDelay = 1 / 3
                NRessource = 1 / 3
                NAvailability = 0
                NLoadBalancing = 1/3

                for i in range(0, generation):
                    gv1.evolve2()
                    minV = float('inf')
                    minIdx = -1
                    deadlineViolationN = 0
                    totalResourceN = 0
                    reliabilityInstanceN = 0
                    TotalObjective = 0
                    loadbalancingN = 0
                    for idx, v in enumerate(gv1.corega.populationPt.fitness):
                        if (gv1.corega.populationPt.maxDeadlineViolation != gv1.corega.populationPt.minDeadlineViolation):
                            deadlineViolationN = NDeadline * (v["deadlineViolation"] - gv1.corega.populationPt.minDeadlineViolation) / (gv1.corega.populationPt.maxDeadlineViolation - gv1.corega.populationPt.minDeadlineViolation)
                        else:
                            deadlineViolationN = v["deadlineViolation"]

                        if (gv1.corega.populationPt.maxRessource != gv1.corega.populationPt.minRessource):
                            totalResourceN = NRessource * (v["totalResource"] - gv1.corega.populationPt.minRessource) / (gv1.corega.populationPt.maxRessource - gv1.corega.populationPt.minRessource)
                        else:
                            totalResourceN = v["totalResource"]

                        if (gv1.corega.populationPt.maxAvailability != gv1.corega.populationPt.minAvailability):
                            reliabilityInstanceN = NAvailability * (v["reliabilityInstance"] - gv1.corega.populationPt.minAvailability) / (gv1.corega.populationPt.maxAvailability - gv1.corega.populationPt.minAvailability)
                        else:
                            reliabilityInstanceN = v["reliabilityInstance"]

                        if (gv1.corega.populationPt.maxloadBalancing != gv1.corega.populationPt.minloadBalancing):
                            loadbalancingN = NLoadBalancing * (v["loadbalancing"] - gv1.corega.populationPt.minloadBalancing) / (gv1.corega.populationPt.maxloadBalancing - gv1.corega.populationPt.minloadBalancing)
                        else:
                            loadbalancingN = v["loadbalancing"]

                        TotalObjective = deadlineViolationN * 1 / 3 + totalResourceN * 1 / 3 + loadbalancingN * 1 / 3
                        gv1.corega.populationPt.fitness[idx]['total'] = TotalObjective

                        if TotalObjective < minV:
                            minIdx = idx
                            minV = TotalObjective
                    currentSolution['population'] = gv1.corega.populationPt.population[minIdx]
                    currentSolution['fitness'] = gv1.corega.populationPt.fitness[minIdx]

                    print("----------------------------------------------------")
                    # print(f"Population  {g.corega.populationPt.population}")
                    # print(f"Fitness  {g.corega.populationPt.fitness}")
                    print(f"Objective  {gv1.corega.populationPt}")
                    print("----------------------------------------------------")

                    generationSolution.append(currentSolution)

                    FinalResult = currentSolution["fitness"]
                    resultValues = str(i) + ";" + str("NSGA2V1") + ";" + str(20) + ";" + str(service) + ";" + str(
                        user) + ";" + str(FinalResult["deadlineViolation"]) + ";" + str(
                        FinalResult["totalResource"]) + ";" + str(FinalResult["loadbalancing"]) + ";" + str(
                        gv1.corega.populationPt.minDeadlineViolation) + ";" + str(
                        gv1.corega.populationPt.minRessource) + ";" + str(
                        gv1.corega.populationPt.minloadBalancing) + ";" + str(
                        i) + ";" + str("0") + ";" + str(system.UsersNumber) + ";" + str(
                        system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) + ";" + str(
                        system.neededinstanceSum[1]) + ";" + str(system.neededinstanceSum[2]) + ";" + str(
                        system.neededinstanceSum[3]) + ";" + str(system.neededinstanceSum[4])
                    agregatedResults.write(resultValues + '\n')
                    agregatedResults.flush()

                # Sort the solution
                generationSolution.sort(key=lambda x: x["fitness"]["total"])
                print("all solution", generationSolution)




                """ 
                ################################################################
                # ---------------------------NSGA-2V2---------------------------
                ################################################################
                """

                generationSolution = list()
                generationPareto = list()

                # Get the min population fitness
                minV = float('inf')
                minIdx = -1

                # --------------------------------------------------------------------------
                currentSolution = {}
                currentSolution['fitness'] = gv2.corega.populationPt.fitness[minIdx]
                currentSolution['population'] = gv2.corega.populationPt.population[minIdx]
                generationSolution.append(currentSolution)

                NDeadline = 1 / 3
                NDelay = 1 / 3
                NRessource = 1 / 3
                NAvailability = 0
                NLoadBalancing = 1/3
                for i in range(0, generation):
                    gv2.evolve2()
                    minV = float('inf')
                    minIdx = -1
                    deadlineViolationN = 0
                    totalResourceN = 0
                    reliabilityInstanceN = 0
                    TotalObjective = 0
                    loadbalancingN = 0
                    for idx, v in enumerate(gv2.corega.populationPt.fitness):
                        if (gv2.corega.populationPt.maxDeadlineViolation != gv2.corega.populationPt.minDeadlineViolation):
                            deadlineViolationN = NDeadline * (v["deadlineViolation"] - gv2.corega.populationPt.minDeadlineViolation) / (gv2.corega.populationPt.maxDeadlineViolation - gv2.corega.populationPt.minDeadlineViolation)
                        else:
                            deadlineViolationN = v["deadlineViolation"]

                        if (gv2.corega.populationPt.maxRessource != gv2.corega.populationPt.minRessource):
                            totalResourceN = NRessource * (v["totalResource"] - gv2.corega.populationPt.minRessource) / (gv2.corega.populationPt.maxRessource - gv2.corega.populationPt.minRessource)
                        else:
                            totalResourceN = v["totalResource"]

                        if (gv2.corega.populationPt.maxAvailability != gv2.corega.populationPt.minAvailability):
                            reliabilityInstanceN = NAvailability * (v["reliabilityInstance"] - gv2.corega.populationPt.minAvailability) / (gv2.corega.populationPt.maxAvailability - gv2.corega.populationPt.minAvailability)
                        else:
                            reliabilityInstanceN = v["reliabilityInstance"]
                        if (gv2.corega.populationPt.maxloadBalancing != gv2.corega.populationPt.minloadBalancing):
                            loadbalancingN = NLoadBalancing * (v["loadbalancing"] - gv2.corega.populationPt.minloadBalancing) / (gv2.corega.populationPt.maxloadBalancing - gv2.corega.populationPt.minloadBalancing)
                        else:
                            loadbalancingN = v["loadbalancing"]

                        TotalObjective = deadlineViolationN * 1 / 3 + totalResourceN * 1 / 3 + loadbalancingN * 1 / 3
                        gv2.corega.populationPt.fitness[idx]['total'] = TotalObjective

                        if TotalObjective < minV:
                            minIdx = idx
                            minV = TotalObjective
                    currentSolution['population'] = gv2.corega.populationPt.population[minIdx]
                    currentSolution['fitness'] = gv2.corega.populationPt.fitness[minIdx]

                    print("----------------------------------------------------")
                    # print(f"Population  {g.corega.populationPt.population}")
                    # print(f"Fitness  {g.corega.populationPt.fitness}")
                    print(f"Objective  {gv2.corega.populationPt}")
                    print("----------------------------------------------------")

                    generationSolution.append(currentSolution)

                    FinalResult = currentSolution["fitness"]
                    resultValues = str(i) + ";" + str("NSGA2V2") + ";" + str(20) + ";" + str(service) + ";" + str(
                        user) + ";" + str(FinalResult["deadlineViolation"]) + ";" + str(
                        FinalResult["totalResource"]) + ";" + str(FinalResult["loadbalancing"]) + ";" + str(
                        gv2.corega.populationPt.minDeadlineViolation) + ";" + str(
                        gv2.corega.populationPt.minRessource) + ";" + str(
                        gv2.corega.populationPt.minloadBalancing) + ";" + str(
                        i) + ";" + str("0") + ";" + str(system.UsersNumber) + ";" + str(
                        system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) + ";" + str(
                        system.neededinstanceSum[1]) + ";" + str(system.neededinstanceSum[2]) + ";" + str(
                        system.neededinstanceSum[3]) + ";" + str(system.neededinstanceSum[4])
                    agregatedResults.write(resultValues + '\n')
                    agregatedResults.flush()

                # Sort the solution
                generationSolution.sort(key=lambda x: x["fitness"]["total"])
                print("all solution", generationSolution)







agregatedResults.close()











