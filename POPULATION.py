import numpy as np
import copy

class POPULATION:
    """
        POPULATION class includes all the information related with a population. The population
        is formed by "size" individuals (chromosome), fitness, fronts, dominators, dominated,
        crowding distances and population Union (P and Q).
    """

    def __init__(self, size):
        self.population = [list()] * size
        self.fitness = [{}] * size
        self.fitnessNormalized = [{}] * size
        self.dominatesTo = [set()] * size
        self.dominatedBy = [set()] * size
        self.fronts = [set()] * size
        self.crowdingDistances = [float(0)] * size
        self.minloadBalancing = float('inf')
        self.maxloadBalancing = 0

        self.minDelay = float('inf')
        self.maxDelay = 0
        self.minRessource = float('inf')
        self.maxRessource = 0
        self.minAvailability = float('inf')
        self.maxAvailability = 0
        self.minDeadlineViolation = float('inf')
        self.maxDeadlineViolation = 0

    def populationUnion(self,p,q):

        """
        Create a new population with the union of other 2 populations
        """
        r=POPULATION(1)
        r.population = copy.deepcopy(p.population) + copy.deepcopy(q.population)
        r.fitness = copy.deepcopy(p.fitness) + copy.deepcopy(q.fitness)
        r.fitnessNormalized = copy.deepcopy(p.fitnessNormalized) + copy.deepcopy(q.fitnessNormalized)
        for i,v in enumerate(r.fitness):
            r.fitness[i]["index"] = i
        r.dominatesTo = copy.deepcopy(p.dominatesTo) + copy.deepcopy(q.dominatesTo)
        r.dominatedBy = copy.deepcopy(p.dominatedBy) + copy.deepcopy(q.dominatedBy)
        r.fronts = copy.deepcopy(p.fronts) + copy.deepcopy(q.fronts)
        r.crowdingDistances = copy.deepcopy(p.crowdingDistances) + copy.deepcopy(q.crowdingDistances)

        r.minloadBalancing = min(p.minloadBalancing,q.minloadBalancing)
        r.maxloadBalancing = max(p.maxloadBalancing,q.maxloadBalancing)
        r.minDelay = min(p.minDelay ,q.minDelay)
        r.maxDelay = max(p.maxDelay,q.maxDelay)
        r.minRessource = min(p.minRessource,q.minRessource)
        r.maxRessource = max(p.maxRessource,q.maxRessource)
        r.minAvailability = min(p.minAvailability,q.minAvailability)
        r.maxAvailability = max(p.maxAvailability,q.maxAvailability)
        r.minDeadlineViolation = min(p.minDeadlineViolation,q.maxDeadlineViolation)
        r.maxDeadlineViolation = max(p.maxDeadlineViolation,q.maxDeadlineViolation)

        return r

    def initNormalizedFunction(self):
        self.minloadBalancing = float('inf')
        self.maxloadBalancing = 0
        self.minDelay = float('inf')
        self.maxDelay = 0
        self.minRessource = float('inf')
        self.maxRessource = 0
        self.minAvailability = float('inf')
        self.maxAvailability = 0
        self.minDeadlineViolation = float('inf')
        self.maxDeadlineViolation = 0

    def NormalizedFunction(self):
        deadlineViolationN = 0
        totalResourceN = 0
        reliabilityInstanceN = 0
        TotalObjective = 0
        loadbalancingN = 0
        #print("wassim",self.fitness)
        for idx, v in enumerate(self.fitness):
            chr_fitness = {}
            if (self.maxDeadlineViolation != self.minDeadlineViolation):
                deadlineViolationN = (v["deadlineViolation"] - self.minDeadlineViolation) / (self.maxDeadlineViolation - self.minDeadlineViolation)
            else:
                deadlineViolationN = v["deadlineViolation"]

            if (self.maxRessource != self.minRessource):
                totalResourceN = (v["totalResource"] - self.minRessource) / (self.maxRessource - self.minRessource)
            else:
                totalResourceN = v["totalResource"]

            if (self.maxAvailability != self.minAvailability):
                reliabilityInstanceN = (v["reliabilityInstance"] - self.minAvailability) / (self.maxAvailability - self.minAvailability)
            else:
                reliabilityInstanceN = v["reliabilityInstance"]

            if (self.maxloadBalancing != self.minloadBalancing):
                loadbalancingN = (v["loadbalancing"] - self.minloadBalancing) / (self.maxloadBalancing - self.minloadBalancing)
            else:
                loadbalancingN = v["loadbalancing"]
            chr_fitness["index"] = idx
            chr_fitness["deadlineViolation"] = np.round(deadlineViolationN,3) if deadlineViolationN != float('inf') else deadlineViolationN
            chr_fitness["totalResource"] = np.round(totalResourceN,3) if totalResourceN != float('inf') else totalResourceN
            chr_fitness["reliabilityInstance"] = np.round(reliabilityInstanceN,3) if reliabilityInstanceN != float('inf') else reliabilityInstanceN
            chr_fitness["loadbalancing"] = np.round(loadbalancingN,3) if loadbalancingN != float('inf') else loadbalancingN

            self.fitnessNormalized[idx] = chr_fitness


        #print("wass",self.fitnessNormalized)



    def __str__(self):
        return "minDeadlineViolation: "+str(self.minDeadlineViolation)+" maxDeadlineViolation: "+str(self.maxDeadlineViolation)+" minRessource: "+str(self.minRessource)+" maxRessource: "+str(self.maxRessource)+" minAvailability: "+str(self.minAvailability)+" maxAvailability: "+str(self.maxAvailability)+" minLoadBalancing: "+str(self.minloadBalancing)+" maxLoadBalancing: "+str(self.maxloadBalancing)

        #return "minDelay: "+str(self.minDelay)+" maxDelay: "+str(self.maxDelay)+" minRessource: "+str(self.minRessource)+" maxRessource: "+str(self.maxRessource)+" minAvailability: "+str(self.minAvailability)+" maxAvailability: "+str(self.maxAvailability)


    

