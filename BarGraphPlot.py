
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import random as rand
import seaborn as sns
import numpy as np

sns.set(style="white", palette="bright", color_codes=True)


file_pathResult = 'plot/result/plot'
calculateReliability = False
numberofGenerations = 300

df = pd.read_csv('Result/Test/agregatedTest303.csv', sep=';')

print(df)
cases = []
cases.append(['ms', 30]) # need to be fix
# cases.append(['user', 3])


for case in cases:

    outterElement = case[0]
    outterValue = case[1]
    metrictitle = ""
    myytitle = ""
    myxtitle = ""

    df2 = df.loc[df[outterElement] == outterValue]
    print(df2)
    # for metric in ['MinD', 'MinR', 'MinLB']:
    #     if metric == 'MinD':
    #         myytitle = 'deadline'
    #         metrictitle = 'deadline violation'
    #     if metric == 'MinR':
    #         myytitle = 'Resource'
    #         metrictitle = 'Resource consumption'
    #     if metric == 'MinLB':
    #         myytitle = 'loadBalancing'
    #         metrictitle = 'load balancing degree'
    for metric in ['deadline', 'resource', 'LB']:
        if metric == 'deadline':
            myytitle = 'deadline'
            metrictitle = 'deadline violation'
        if metric == 'resource':
            myytitle = 'Resource'
            metrictitle = 'Resource consumption'
        if metric == 'LB':
            myytitle = 'loadBalancing'
            metrictitle = 'load balancing degree'

        if outterElement == 'ms':
            figtitleStr = metrictitle + " (" + str(outterValue) + " Services)"
            innerElement = 'requestRate'
            myxtitle = 'Request Rate'
        if outterElement == 'user':
            figtitleStr = metrictitle + " (" + str(outterValue) + " users)"
            innerElement = 'ms'
            myxtitle = 'Number of microservices'

        dfMetricNSGA2 = df2.loc[df['algo'] == 'NSGA2', [innerElement, metric]]
        dfMetricNSGA2v1 = df2.loc[df['algo'] == 'NSGA2V1', [innerElement, metric]]
        dfMetricNSGA2v2 = df2.loc[df['algo'] == 'NSGA2V2', [innerElement, metric]]
        dfMetricKUBDeadline = df2.loc[df['algo'] == 'KubDealine', [innerElement, metric]]

        dfMetricRequestRate = df2.loc[df['requestRate'] == 'requestRate', [innerElement, metric]]

        # dfMetricNSGA2 = dfMetricNSGA2.rename(columns={'1551': 'NSGA2'})
        # dfMetricNSGA2v1 = dfMetricNSGA2v1.rename(columns={metric: 'NSGA2V1'})
        # dfMetricNSGA2v2 = dfMetricNSGA2v2.rename(columns={metric: 'NSGA2V2'})
        # dfMetricKUBDeadline = dfMetricKUBDeadline.rename(columns={metric: 'KubDealine'})

        print("---------------------------------------")

        dfMetricNSGA2 = dfMetricNSGA2.replace([np.inf, -np.inf], 0) # if inf then 0
        dfMetricNSGA2v1 = dfMetricNSGA2v1.replace([np.inf, -np.inf], 0) # if inf then 0
        dfMetricNSGA2v2 = dfMetricNSGA2v2.replace([np.inf, -np.inf], 0) # if inf then 0
        dfMetricKUBDeadline = dfMetricKUBDeadline.replace([np.inf, -np.inf], 0)


        print(dfMetricKUBDeadline[metric].min())
        minValuey = min(dfMetricNSGA2[metric].min(), dfMetricKUBDeadline[metric].min(),dfMetricNSGA2v1[metric].min(),dfMetricNSGA2v2[metric].min())
        maxValuey = max(dfMetricNSGA2[metric].max(), dfMetricKUBDeadline[metric].max(),dfMetricNSGA2v1[metric].max(),dfMetricNSGA2v2[metric].max())
        minLimity = minValuey - 0.1 * (maxValuey - minValuey)
        maxLimity = maxValuey + 0.1 * (maxValuey - minValuey)

        minLimity = max(0.0, minLimity)

        plt.style.use('seaborn-bright')
        fig, ax = plt.subplots(figsize=(8.0, 4.0))
        index = np.arange(len(list(dfMetricNSGA2[metric])))
        bar_width = 0.2
        opacity = 0.8

        # fig.suptitle(figtitleStr, fontsize=18)

        print("-------------")
        # print(list(dfMetricKUB[metric]))
        print(index + bar_width)
        rects1 = plt.bar(index,np.array(list(dfMetricNSGA2[metric])), bar_width,alpha=opacity,label='SPMOv1')
        rects3 = plt.bar(index + bar_width,  np.array(list(dfMetricNSGA2v2[metric])), bar_width,alpha=opacity,label='SPMOv2')
        rects2 = plt.bar(index + 2 * bar_width, np.array(list(dfMetricKUBDeadline[metric])), bar_width,alpha=opacity,label='KUBDealine')
        rects4 = plt.bar(index + 3 * bar_width, np.array(list(dfMetricNSGA2v1[metric])), bar_width, alpha=opacity, label='NSGA2')


        plt.xticks(index + 3*bar_width / 2, list(dfMetricNSGA2[innerElement]))

        if (metric == 'deadline'):
            plt.ylim([minLimity, 8])
        else:
            plt.ylim([minLimity, maxLimity])

        ax.set_xlabel(myxtitle, fontsize=18)

        ax.set_ylabel(myytitle, fontsize=18)
        # plt.ylim([0,2000])

        # plt.legend(loc="upper center", ncol=4, fontsize=14, bbox_to_anchor=(0.45, 1.12))
        plt.legend(fontsize=11)

        plt.xticks(fontsize=12)
        plt.yticks(fontsize=12)

        plt.grid()

        plt.show()

        fig.savefig(file_pathResult + '/pdf/Bar-'+outterElement+"-"+metric+'.png',format='png')

        plt.close(fig)









