import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import random as rand
import seaborn as sns
import numpy as np

sns.set(style="white", palette="bright", color_codes=True)


file_pathResult = 'plot/result/plot'
calculateReliability = False
numberofGenerations = 300

df = pd.read_csv('Result/Test/agregatedTest301.csv', sep=';')

#print(df)
cases = []
# cases.append(['ms', 30]) # need to be fix
cases.append(['requestRate', 10000])


for case in cases:

    outterElement = case[0]
    outterValue = case[1]
    metrictitle = ""
    myytitle = ""
    myxtitle = ""

    df2 = df.loc[df[outterElement] == outterValue]
    print(df2)
    # for metric in ['MinD', 'MinR', 'MinLB']:
    #     if metric == 'MinD':
    #         myytitle = 'deadline'
    #         metrictitle = 'deadline violation'
    #     if metric == 'MinR':
    #         myytitle = 'Resource'
    #         metrictitle = 'Resource consumption'
    #     if metric == 'MinLB':
    #         myytitle = 'loadBalancing'
    #         metrictitle = 'load balancing degree'
    for metric in ['deadline', 'resource', 'LB']:

        if metric == 'deadline':
            myytitle = 'deadline'
            metrictitle = 'deadline violation'
        if metric == 'resource':
            myytitle = 'Resource'
            metrictitle = 'Resource consumption'
        if metric == 'LB':
            myytitle = 'loadBalancing'
            metrictitle = 'load balancing degree'

        if outterElement == 'ms':
            figtitleStr = metrictitle + " (" + str(outterValue) + " Services)"
            innerElement = 'requestRate'
            myxtitle = 'Request Rate'
        if outterElement == 'requestRate':
            figtitleStr = metrictitle + " (" + str(outterValue) + " users)"
            innerElement = 'ms'
            myxtitle = 'Number of microservices'

        dfMetricNSGA2 = df2.loc[df['algo'] == 'NSGA2', [innerElement, metric]]
        dfMetricNSGA2v1 = df2.loc[df['algo'] == 'NSGA2V1', [innerElement, metric]]
        dfMetricNSGA2v2 = df2.loc[df['algo'] == 'NSGA2V2', [innerElement, metric]]
        dfMetricKUBDeadline = df2.loc[df['algo'] == 'KubDealine', [innerElement, metric]]

        dfMetricRequestRate = df2.loc[df['requestRate'] == 'requestRate', [innerElement, metric]]


        print("---------------------------------------")

        dfMetricNSGA2 = dfMetricNSGA2.replace([np.inf, -np.inf], 0) # if inf then 0
        dfMetricNSGA2v1 = dfMetricNSGA2v1.replace([np.inf, -np.inf], 0) # if inf then 0
        dfMetricNSGA2v2 = dfMetricNSGA2v2.replace([np.inf, -np.inf], 0) # if inf then 0

        dfMetricKUBDeadline = dfMetricKUBDeadline.replace([np.inf, -np.inf], 0)


        print(dfMetricKUBDeadline[metric].min())
        minValuey = min(dfMetricNSGA2[metric].min(), dfMetricKUBDeadline[metric].min(),dfMetricNSGA2v1[metric].min(),dfMetricNSGA2v2[metric].min())
        maxValuey = max(dfMetricNSGA2[metric].max(), dfMetricKUBDeadline[metric].max(),dfMetricNSGA2v1[metric].max(),dfMetricNSGA2v2[metric].max())
        minLimity = minValuey - 0.1 * (maxValuey - minValuey)
        maxLimity = maxValuey + 0.1 * (maxValuey - minValuey)

        minLimity = max(0.0, minLimity)
        #minLimity = 0.0

        fig = plt.figure()

        titlecase = "5000-10000 Request"
        # fig.suptitle(metrictitle + " (" + titlecase + ")", fontsize=18)
        ax = fig.add_subplot(111)

        ax.set_xlabel(myxtitle, fontsize=19)
        ax.set_ylabel(myytitle, fontsize=19)
        plt.gcf().subplots_adjust(left=0.15)
        # ax.plot(list(dfMetricNSGA2[metric]), label='NSGA2', linewidth=2.0,marker='o')
        # ax.plot(list(dfMetricKUBDeadline[metric]), label='KubDealine',linewidth=2.0, linestyle="-.")
        # ax.plot(list(dfMetricNSGA2v2[metric]), label='NSGA2v2', linewidth=2.0,linestyle="-.")
        # ax.plot(list(dfMetricNSGA2v1[metric]), label='NSGA2v1', linewidth=2.0,linestyle="-.")


        ax.plot([10,20,30,40,50],list(dfMetricNSGA2[metric]), label='SPMOv1', linewidth=2.0,marker='o')
        ax.plot([10,20,30,40,50], list(dfMetricNSGA2v2[metric]), label='SPMOv2', linewidth=2.0, marker='s')
        ax.plot([10,20,30,40,50],list(dfMetricKUBDeadline[metric]), label='KubDealine', linewidth=2.0, marker='v')
        ax.plot([10,20,30,40,50],list(dfMetricNSGA2v1[metric]), label='NSGA2', linewidth=2.0,marker='^')

        #ax.plot(list(dfMetricKUB[innerElement]),list(dfMetricKUB[metric]), label='Kub', linewidth=2.0, linestyle="--",marker='s')

        print(dfMetricKUBDeadline)
        plt.legend(loc="upper center", ncol=4, fontsize=14, bbox_to_anchor=(0.45, 1.12))
        #plt.ylim([minLimity, maxLimity])
        # plt.xlim([minLimitx, maxLimitx])


        plt.legend(fontsize=12)
        plt.yticks(fontsize=14)
        plt.xticks(fontsize=14)
        #plt.xticks(list(dfMetricKUB[innerElement]))


        #plt.grid()
        plt.show()
        fig.savefig(file_pathResult + '/Plot'+outterElement+"-"+metric+'.png',format='png')
        plt.close(fig)

















