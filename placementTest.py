
import random
import SystemConfiguration
import MyConfig
import NSGA2 as nsga2
import NetworkPlot
import pickle
import sys
import json
import Kub
from datetime import datetime



experimentList = []
# 3 - mutation probability 0-1.0
# 4 - population size
# 5 - seed for random values for generation of the initial population
# 6 - seed for random values for evolution of populations
experimentList.append([100,500,888,0.25])

ServiceScaleLevel = [10,20,30,40,50]
UserScaleLevel = [2,4,5,6,7,9,10,11,12,13,14,15,17,20,22,26,29,21,23,25,26]
performance = [80,8]

generationList = [20,50,100]
file_path = "Result"
agregatedResults = open(file_path+'/agregatedResults.csv', 'w')

#resultValues = "ex;algo;fog;ms;user;deadline;resource;LB;MinD;MinR;MinLB;generation;numberOfUsers;maxNeededInstance;requestRate;"
resultValues = "ex;algo;fog;ms;user;numberOfUsers;maxNeededInstance;requestRate;1;2;3;4"

agregatedResults.write(resultValues.replace(".",",")+'\n')
agregatedResults.flush()
i = 0
for ex in experimentList:
    i = i + 1
    for service in ServiceScaleLevel:
        for user in UserScaleLevel:
            experimentName = "ex- " + str(i) + " G- "
            print("***********************************************************")
            # print("*" + experimentName + " :::: " + str(datetime.now()))
            # print("***********************************************************")

            # ****************************************************************************************************
            # inizializations and set up
            # ****************************************************************************************************

            cnf_ = MyConfig.myConfig(ex[0], ex[1], ex[2], ex[3], 20)  # Population and generation configuration

            system = SystemConfiguration.SystemConfiguration(cnf_, service)  # serviceNumber 2, maxuserPerGateway 3

            system.networkGeneration()  # Fog infrastructure
            # system.GraphPlot() # plot fog
            system.serviceGeneration()  # microservice application
            system.userGeneration(user)  # users placement

            print("user ",str(user),"service ",service, "NeededInstance ",system.neededinstanceSum)

            resultValues = str(i) + ";" + str("NSGA2") + ";" + str(20) + ";" + str(service) + ";" + str(user) + ";" + str(system.UsersNumber) + ";" + str(system.MaxNeededInstance) + ";" + str(system.reqestRateTotal) + ";" + str(system.neededinstanceSum[1])+ ";" + str(system.neededinstanceSum[2])+ ";" + str(system.neededinstanceSum[3])+ ";" + str(system.neededinstanceSum[4])
            agregatedResults.write(resultValues.replace(".", ",") + '\n')
            agregatedResults.flush()


            for generation in generationList:

                # --------------------------------------------------------------------------
                # Kubernetes solution
                # ---------------------------------------------------------------------------

                # kubernetes = Kub.Kub(system,cnf_)
                # kubernetes.scheduling()

                # --------------------------------------------------------------------------
                # NSGA2 solution
                # ---------------------------------------------------------------------------

                """
                g = nsga2.NSGA2(system, cnf_)

                generationSolution = list()
                generationPareto = list()

                # print(f"Population Zero {g.corega.populationPt.population}")
                # print(f"Fitness Zero {g.corega.populationPt.fitness}")

                # to get the min population fitness
                minV = float('inf')
                minIdx = -1
                # --------------------------------------------------------------------------

                currentSolution = {}
                currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]
                currentSolution['population'] = g.corega.populationPt.population[minIdx]
                generationSolution.append(currentSolution)

                NDeadline = 1 / 3
                NDelay = 1 / 3
                NRessource = 1 / 3
                NAvailability = 0

                # ------------------------------DEADLINE VIOLATION----------------------------

                for i in range(0, generation):
                    g.evolve2()
                    minV = float('inf')
                    minIdx = -1
                    deadlineViolationN = 0
                    totalResourceN = 0
                    reliabilityInstanceN = 0
                    TotalObjective = 0
                    loadbalancingF = 0
                    for idx, v in enumerate(g.corega.populationPt.fitness):
                        if (g.corega.populationPt.maxDeadlineViolation != g.corega.populationPt.maxDeadlineViolation):
                            deadlineViolationN = NDeadline * (v["deadlineViolation"] - g.corega.populationPt.minDeadlineViolation) / (g.corega.populationPt.maxDeadlineViolation - g.corega.populationPt.minDeadlineViolation)
                        else:
                            deadlineViolationN = v["deadlineViolation"]

                        if (g.corega.populationPt.maxRessource != g.corega.populationPt.minRessource):
                            totalResourceN = NRessource * (v["totalResource"] - g.corega.populationPt.minRessource) / (g.corega.populationPt.maxRessource - g.corega.populationPt.minRessource)
                        else:
                            totalResourceN = v["totalResource"]

                        if (g.corega.populationPt.maxAvailability != g.corega.populationPt.minAvailability):
                            reliabilityInstanceN = NAvailability * (v["reliabilityInstance"] - g.corega.populationPt.minAvailability) / (g.corega.populationPt.maxAvailability - g.corega.populationPt.minAvailability)
                        else:
                            reliabilityInstanceN = v["reliabilityInstance"]
                        loadbalancingF = v["loadbalancing"]

                        TotalObjective = deadlineViolationN + totalResourceN
                        g.corega.populationPt.fitness[idx]['total'] = TotalObjective

                        if TotalObjective < minV:
                            minIdx = idx
                            minV = TotalObjective
                    currentSolution['population'] = g.corega.populationPt.population[minIdx]
                    currentSolution['fitness'] = g.corega.populationPt.fitness[minIdx]

                    print("----------------------------------------------------")
                    # print(f"Population  {g.corega.populationPt.population}")
                    #print(f"Fitness  {g.corega.populationPt.fitness}")
                    print(f"Objective  {g.corega.populationPt}")
                    print("----------------------------------------------------")

                    generationSolution.append(currentSolution)

                
                # Sort the solution
                generationSolution.sort(key=lambda x: x["fitness"]["total"])
                print("all solution", generationSolution)

                # Network plot

                # SolutionPlot = NetworkPlot.NetworkPlot(system, cnf_, generationSolution, {})
                # SolutionPlot.plotNSGA2()
                

                # export then result to csv
                #FinalResult = generationSolution[0]["fitness"]
                resultValues = str(i) + ";" + str("NSGA2") + ";" + str(20) + ";" + str(service) + ";"+str(user) + ";" + str(FinalResult["deadlineViolation"]) + ";" + str(FinalResult["totalResource"]) + ";" + str(round(FinalResult["loadbalancing"])) +";"+str(g.corega.populationPt.minDeadlineViolation)+";"+str(g.corega.populationPt.minRessource)+";"+str(g.corega.populationPt.loadBalancing)+";"+str(generation) +";"+str(system.UsersNumber)
                agregatedResults.write(resultValues.replace(".", ",") + '\n')
                agregatedResults.flush()

                # output = open(file_path + '/' + "firstTry" + '-lastGeneration.pkl', 'wb')
                # pickle.dump(g.corega.populationPt, output)
                # output.close()
                
                """

agregatedResults.close()











