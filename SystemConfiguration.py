import networkx as nx
import time
import operator
import copy
import json
import random
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import collections


class SystemConfiguration:

    def __init__(self, cnf_,servicesNumber):

        # FOG INFRASTRUCTURE
        self.func_PROPAGATION = "random.randint(1,1)"  # MS
        self.func_BANDWITDH = "random.randint(80,100)"  # MB/S
        self.func_NETWORKGENERATION = "nx.barabasi_albert_graph(n=20, m=2, seed=200)"  # algorithm for the generation of the network topology
        self.func_NODERAM = "random.randint(4096,8192)"  # MB RAM
        self.func_NODECPU = "random.randint(16,32)"  # CPU / CORE

        # SERVICES
        self.TOTALNUMBEROFAPPS = servicesNumber
        self.serviceNumber = self.TOTALNUMBEROFAPPS
        self.func_SERVICECPU = "random.randint(1,4)"  # CORE
        self.func_SERVICERAM = "random.randint(100,400)"  # MB
        self.func_SERVICERATE = "random.randint(100,200)"  # RPS
        self.func_SERVICEDEADLINE = "random.randint(4,8)"
        # USERS and IoT DEVICES
        self.func_REQUESTSIZE = "random.randint(1,5)"  # MB
        self.func_REQUESTRATE = "random.randint(5,10)"  # RPS
        self.func_REQUESTPROB = "1/5"
        self.func_REQUESTACCESSDELAY= "random.randint(0,1)"

        self.cnf = cnf_

        self.UsersJson = 'Configuration/test/Users.json'
        self.AppJson = 'Configuration/test/ServiceConfig.json'

    def networkGeneration(self):

        # ****************************************************************************************************
        # The Fog Edge Network Topology
        # ****************************************************************************************************

        self.G = eval(self.func_NETWORKGENERATION)
        self.fogDevices = list()  # fog nodes
        self.fogEdges = list()  # fog edges
        self.nodeRAM = {}
        self.nodeCPU = {}
        # self.CPUTotal = 0
        # self.RAMTotal = 0
        self.fogNumber = len(self.G.nodes)
        self.fogBandwidthMatrix = [[0 for j in range(self.fogNumber)] for i in range(self.fogNumber)]  # to calculate the shortest path between node

        for e in self.G.edges:
            self.G[e[0]][e[1]]['PR'] = eval(self.func_PROPAGATION)
            self.G[e[0]][e[1]]['BW'] = round(1 / eval(self.func_BANDWITDH), 2)

        for i in range(0, len(self.G.nodes)):
            self.nodeRAM[i] = eval(self.func_NODERAM)
            self.nodeCPU[i] = eval(self.func_NODECPU)
            # self.CPUTotal = self.CPUTotal + self.nodeCPU[i]
            # self.RAMTotal = self.RAMTotal + self.nodeRAM[i]

            for j in range(i, len(self.G.nodes)):
                weight = nx.shortest_path_length(self.G, source=i, target=j, weight='BW')
                self.fogBandwidthMatrix[i][j] = weight
                self.fogBandwidthMatrix[j][i] = weight

        # JSON EXPORT
        netJson = {}

        for i in self.G.nodes:
            myNode = {}
            myNode['id'] = i
            myNode['RAM'] = self.nodeRAM[i]
            myNode['CPU'] = self.nodeCPU[i]
            self.fogDevices.append(myNode)

        for e in self.G.edges:
            myLink = {}
            myLink['s'] = e[0]
            myLink['d'] = e[1]
            myLink['PR'] = self.G[e[0]][e[1]]['PR']
            myLink['BW'] = self.G[e[0]][e[1]]['BW']
            self.fogEdges.append(myLink)

        netJson['node'] = self.fogDevices
        netJson['link'] = self.fogEdges

        self.allShortestPath = nx.shortest_path(self.G, weight="BW", method='dijkstra')  # display the path
        # print("shortest path", self.allShortestPath)

        self.allShortestPathLength = dict(
            nx.shortest_path_length(self.G, weight='BW', method='dijkstra'))  # display the length
        # print("shortest path", dict(self.allShortestPathLength))

        file = open(self.cnf.resultFolder + "/FogNetworkDefinition.json", "w")
        file.write(json.dumps(netJson))
        file.close()

    def serviceGeneration(self):

        # ******************************************************************************************
        #   Service definition
        # ******************************************************************************************

        serviceJson = list()
        self.serviceResourcesRAM = list()
        self.serviceResourcesCPU = list()

        for i in range(0, self.TOTALNUMBEROFAPPS):
            serviceObject = collections.defaultdict(dict)
            serviceObject["serviceId"] = i
            serviceObject["resource"]["RAM"] = eval(self.func_SERVICERAM)
            serviceObject["resource"]["CPU"] = eval(self.func_SERVICECPU)
            serviceObject["resource"]["ServiceRate"] = eval(self.func_SERVICERATE)
            serviceObject["resource"]["Deadline"] = eval(self.func_SERVICEDEADLINE)
            self.serviceResourcesRAM.append(serviceObject["resource"]["RAM"])
            self.serviceResourcesCPU.append(serviceObject["resource"]["CPU"])
            serviceJson.append(serviceObject)

        self.AppConfig = serviceJson  # service struture

        file = open(self.cnf.resultFolder + "/ServiceConfiguration.json", "w")
        file.write(json.dumps(serviceJson))
        file.close()

    def userGeneration(self,userPerGateway):
        # ******************************************************************************************
        #   User connection/gateways definition
        # ******************************************************************************************
        self.USERPERGATEWAY = userPerGateway # USER PER GATEWAY

        ServiceDemandJson = list()

        SumNeededInstances = 0
        requestRate = 0
        probOfRequested = eval(self.func_REQUESTPROB)
        placed = False
        self.UsersNumber = 0
        self.MaxNeededInstance = 0
        self.reqestRateTotal = 0
        self.neededinstanceSum = {0:0,1:0,2:0,3:0,4:0}
        gatewaysNumber = 1

        for i in range(0, self.TOTALNUMBEROFAPPS):
            UserObject = {
            "id": 0,
            "device": {
                    "serviceId": 0,
                    "gatewayId": [],
                    "AccessDelay": [],
                    "Requestrate": [],
                    "Data_in": 0,
                    "NeededInstance": [],
                    "SumNeededInstances": 0
                }

            }
            UserObject["id"] = i
            UserObject["device"]["serviceId"] = i
            UserObject["device"]["Data_in"] = eval(self.func_REQUESTSIZE)
            if (gatewaysNumber > 4):
                gatewaysNumber = 1
            gatewaysNumber = gatewaysNumber + 1

            gateways = gatewaysNumber
            #gateways = random.randint(2,self.fogNumber * probOfRequested)
            gatewayList = []
            while gateways > 0 :
                gateway = random.randint(0,self.fogNumber-1)
                if gateway not in gatewayList :
                    gatewayList.append(gateway)

                    userPerGateway = random.randint(2,self.USERPERGATEWAY)
                    for k in range(0,userPerGateway):
                        requestRate = requestRate + eval(self.func_REQUESTRATE)
                        self.UsersNumber = self.UsersNumber  + 1
                    self.reqestRateTotal = self.reqestRateTotal + requestRate
                    NeededInstance = round(requestRate / self.AppConfig[i]["resource"]["ServiceRate"]) + 1
                    self.MaxNeededInstance = max(self.MaxNeededInstance, NeededInstance)
                    self.neededinstanceSum[NeededInstance]= self.neededinstanceSum[NeededInstance] + 1
                    UserObject["device"]["gatewayId"].append(gateway)
                    UserObject["device"]["AccessDelay"].append(eval(self.func_REQUESTACCESSDELAY))
                    UserObject["device"]["NeededInstance"].append(NeededInstance)
                    UserObject["device"]["Requestrate"].append(requestRate)
                    SumNeededInstances = SumNeededInstances + NeededInstance
                    requestRate = 0
                    gateways = gateways - 1

            UserObject["device"]["SumNeededInstances"] = SumNeededInstances
            SumNeededInstances = 0
            ServiceDemandJson.append(UserObject)

        self.UsersJ = ServiceDemandJson

        self.arrayMaxNeededInstances = [0 for j in range(self.serviceNumber)]

        self.arrayMaxNeededInstancesPositions = [0 for j in range(self.serviceNumber + 1)]

        self.IoTDevicePlacementMatrix = [list() for i in range(self.TOTALNUMBEROFAPPS)]

        for User in ServiceDemandJson:
            self.arrayMaxNeededInstances[User["device"]["serviceId"]] = max(User["device"]["NeededInstance"])
            self.arrayMaxNeededInstancesPositions[User["device"]["serviceId"] + 1] = max(User["device"]["NeededInstance"]) + self.arrayMaxNeededInstancesPositions[User["device"]["serviceId"]]
            self.IoTDevicePlacementMatrix[User["device"]["serviceId"]].extend(User["device"]["gatewayId"])

        # print("arrayMaxNeededInstances",self.arrayMaxNeededInstances)
        # print("arrayMaxNeededInstancesPositions",self.arrayMaxNeededInstancesPositions)
        # print("IoTDevicePlacementMatrix",self.IoTDevicePlacementMatrix)


        file = open(self.cnf.resultFolder + "/UserConfiguration.json", "w")
        file.write(json.dumps(ServiceDemandJson))
        file.close()


    def GraphPlot(self):
        val_map = {'A': 1.0, 'D': 0.5714285714285714, 'H': 0.0}
        labels = nx.get_edge_attributes(self.G, 'BW')
        # values = [val_map.get(node, 0.25) for node in self.G.nodes()]
        pos = nx.spring_layout(self.G)
        nx.draw_networkx_nodes(self.G, pos, cmap=plt.get_cmap('jet'), node_color='r', node_size=500)
        nx.draw_networkx_labels(self.G, pos)
        nx.draw_networkx_edge_labels(self.G, pos, edge_labels=labels)
        nx.draw_networkx_edges(self.G, pos, edgelist=self.G.edges, edge_color='b', arrows=False)
        # nx.draw_spring(self.G)
        plt.show()

    def networkGenerationTEST(self):

        # ****************************************************************************************************
        # The Fog Edge Network Topology
        # ****************************************************************************************************

        self.G = nx.Graph()
        self.G.add_nodes_from([0, 1, 2, 3, 4, 5])
        self.G.add_edge(0, 1)
        self.G.add_edge(0, 5)
        self.G.add_edge(1, 2)
        self.G.add_edge(2, 3)
        self.G.add_edge(3, 4)
        self.G.add_edge(4, 5)
        self.G.add_edge(4, 1)
        self.fogdevices = list()
        self.nodeRAM = {}
        self.nodeCPU = {}
        self.fogNumber = len(self.G.nodes)
        self.fogBandwidthMatrix = [[0 for j in range(self.fogNumber)] for i in range(self.fogNumber)]  # to calculate the shortest path between node
        self.fogResources = list()

        for e in self.G.edges:
            self.G[e[0]][e[1]]['PR'] = eval(self.func_PROPAGATION)
            # self.G[e[0]][e[1]]['BW'] = 1 / eval(self.func_BANDWITDH)
            self.G[e[0]][e[1]]['BW'] = 1 / 100

        for i in range(0, len(self.G.nodes)):
            self.nodeRAM[i] = eval(self.func_NODERAM)
            self.nodeCPU[i] = eval(self.func_NODECPU)
            self.fogResources.append(self.nodeRAM[i])
            for j in range(i, len(self.G.nodes)):
                weight = nx.shortest_path_length(self.G, source=i, target=j, weight='BW')
                self.fogBandwidthMatrix[i][j] = weight
                self.fogBandwidthMatrix[j][i] = weight

        # print(f"latency matrix {self.fogLatencyMatrix}")

        # JSON EXPORT
        netJson = {}

        for i in self.G.nodes:
            myNode = {}
            myNode['id'] = i
            myNode['RAM'] = self.nodeRAM[i]
            myNode['CPU'] = self.nodeCPU[i]
            self.fogdevices.append(myNode)

        myEdges = list()
        for e in self.G.edges:
            myLink = {}
            myLink['s'] = e[0]
            myLink['d'] = e[1]
            myLink['PR'] = self.G[e[0]][e[1]]['PR']
            myLink['BW'] = self.G[e[0]][e[1]]['BW']

            myEdges.append(myLink)

        self.averagePathLength = nx.average_shortest_path_length(self.G, weight="BW")
        # print(self.averagePathLength)
        self.totalResources = np.sum(self.fogResources[0:self.fogNumber])

        netJson['node'] = self.fogdevices
        netJson['link'] = myEdges

        self.allShortestPath = nx.shortest_path(self.G, weight="BW", method='dijkstra')
        # print("shortest path", self.allShortestPath)

        self.allShortestPathLength = dict(nx.shortest_path_length(self.G, weight='BW', method='dijkstra'))
        # print("shortest path", dict(self.allShortestPathLength))

        file = open(self.cnf.resultFolderTEST + "/FofNetworkDefinitionTEST.json", "w")
        file.write(json.dumps(netJson))
        file.close()

    def userGenerationTEST(self):
        # ******************************************************************************************
        #   User connection/gateways definition
        # ******************************************************************************************

        f = open(self.UsersJson, 'r')
        txtjson = f.read()
        f.close()
        myjson = json.loads(txtjson)

        self.UsersJ = myjson

        self.arrayMaxNeededInstances = [0 for j in range(self.serviceNumber)]

        self.arrayMaxNeededInstancesPositions = [0 for j in range(self.serviceNumber + 1)]

        self.IoTDevicePlacementMatrix = [list() for i in range(self.TOTALNUMBEROFAPPS)]

        for User in myjson:
            self.arrayMaxNeededInstances[User["device"]["serviceId"]] = max(User["device"]["NeededInstance"])
            self.arrayMaxNeededInstancesPositions[User["device"]["serviceId"] + 1] = max(
                User["device"]["NeededInstance"]) + self.arrayMaxNeededInstancesPositions[User["device"]["serviceId"]]

            self.IoTDevicePlacementMatrix[User["device"]["serviceId"]].extend(User["device"]["gatewayId"])

        # print(self.IoTDevicePlacementMatrix)

    def serviceGenerationTEST(self):
        # ******************************************************************************************
        #   app definition
        # ******************************************************************************************

        f = open(self.AppJson, 'r')
        txtjson = f.read()
        f.close()
        Appjson = json.loads(txtjson)

        self.serviceResourcesRAM = list()
        self.serviceResourcesCPU = list()

        for service in Appjson:
            self.serviceResourcesRAM.append(service["resource"]["RAM"])
            self.serviceResourcesCPU.append(service["resource"]["CPU"])
        self.AppConfig = Appjson  # App struture

        # print(self.serviceResourcesRAM)