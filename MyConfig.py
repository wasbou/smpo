

class myConfig:

    def __init__(self,populationSize,populationSeed,evolutionSeed,mutationProbability,numberGenerations):
        self.populationSize = populationSize # 100
        self.mutationProbability = mutationProbability #0.25
        self.numberGenerations = numberGenerations #20

        self.myConfiguration_ = 'MicroserviceLoadBalancingJournal'
        self.resultFolder = 'Configuration'
        self.resultFolderTEST = 'Configuration/test'

        self.populationSeed = populationSeed #500
        self.evolutionSeed = evolutionSeed #444
