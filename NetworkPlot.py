
import networkx as nx
import time
import operator
import copy
import json
import random
import matplotlib
import matplotlib.pyplot as plt
import pyvis
from pyvis.network import Network

class NetworkPlot:

    def __init__(self,system,conf,resultnsga2,resultkub):
        self.system = system
        self.cnf = conf
        self.populationSize = self.cnf.populationSize
        self.resultnsga2 = resultnsga2
        self.resultkub = resultkub



    def plotNSGA2(self):
        chromosome = self.resultnsga2[0]["population"]
        print("chromosome",chromosome)
        for MServiceId in range(self.system.serviceNumber):
            self.net = Network(notebook=True)
            for i in range(self.system.arrayMaxNeededInstancesPositions[MServiceId],self.system.arrayMaxNeededInstancesPositions[MServiceId + 1]):
                for fog in range(self.system.fogNumber):
                    if chromosome[i][fog]==1:
                        if (fog in self.system.UsersJ[MServiceId]["device"]["gatewayId"]):
                            self.net.add_node(fog, label=str(fog) + " |user|", size=12, color='#f24f07')
                        else:
                            self.net.add_node(fog, shape = "dot", size = 12, color='#f24f07')
                    elif(fog in self.system.UsersJ[MServiceId]["device"]["gatewayId"]):
                        self.net.add_node(fog, label=str(fog) + " |user|", size=12)

            self.net.from_nx(self.system.G)
            #net.barnes_hut()
            self.net.toggle_physics(status=False)
            #self.net.show_buttons()

            self.net.show("plot/result/Service "+str(MServiceId)+".html")

    def plotkub(self):
        print("result",self.resultkub)
        for MServiceId in range(self.system.serviceNumber):
            self.net = Network(notebook=True)
            for fog in self.resultkub[MServiceId]:
                if (fog in self.system.UsersJ[MServiceId]["device"]["gatewayId"]):
                    self.net.add_node(fog, label=str(fog) + " |user|", size=12, color='#f24f07')
                else:
                    self.net.add_node(fog, shape = "dot", size = 12, color='#f24f07')
            self.net.from_nx(self.system.G)
            #net.barnes_hut()
            self.net.toggle_physics(status=False)
            #self.net.show_buttons()

            self.net.show("plot/result/kub/Service "+str(MServiceId)+".html")
